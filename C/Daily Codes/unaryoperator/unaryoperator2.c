#include<stdio.h>
void main()
{
	int x = 5;
	int y = 10;
	int ans1, ans2;

	ans1 = ++x;			
	printf("++x: %d\n", ++x);	//7
	printf("x: %d\n",x);		//7
	printf("ans1: %d\n", ans1);	//6
	
	ans2 = y++;
	printf("y++: %d\n", y++);	//11
	printf("y: %d\n", y);		//13
	printf("ans2: %d\n", ans2);	//10
}

