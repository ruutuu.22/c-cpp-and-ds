#include<stdio.h>
void main()
{
	int x1 = 5;
	int x2 = 10;
	int output1;
	int output2;
	printf("x1: %d\n", x1);
	printf("output1: %d\n", output1);
	output1 = ++x1;
	printf("x1: %d\n", x1);
	printf("output1: %d\n\n", output1);

	printf("x2: %d\n", x2);   
	printf("output2: %d\n", output2); 
	output2 = x2++;
	printf("x2: %d\n", x2);
	printf("output2: %d\n", output2); 
}
