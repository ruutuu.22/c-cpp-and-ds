/*
 * 1  3  5 
 * 7  9 
 * 11
 */

#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		for(int j=rows; j>=i;j--){
			printf("%d\t", x);
	       		x = x + 2;
		}
		printf("\n");
	}
}	
