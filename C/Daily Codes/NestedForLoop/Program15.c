/*
 *       A
 *     B C
 *   D E F
 * G H I J
 */

#include<stdio.h>
void main(){
	int rows;
	char x = 'A';
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr = 1; ritr<=rows; ritr++){
		for(int sitr = rows -1; sitr>=ritr; sitr--){
			printf("  ");
		}
		for(int citr = 1; citr <= ritr; citr++){
			printf("%c ", x);
			x++;
		}
		printf("\n");
	}
}
