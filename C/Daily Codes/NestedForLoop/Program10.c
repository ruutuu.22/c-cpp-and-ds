/*
 * A B C
 * D E
 * F
 */

#include<stdio.h>
void main(){
	int rows;
	char x ='A';
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		for(int j=rows; j>=i; j--){
			printf("%c\t", x);
			x++;
		}
		printf("\n");
	}
}
