
#include<stdio.h>
void main(){
	int rows;
	printf("Enter row: \n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		for(int j=(rows/2); j>=i; j--){
			printf("  ");
		}
		if(i>rows/2){
			for(int k=(rows/2)-1;k>=rows-i; k--){
				printf("  ");
			}
		}
		for(int l=1; l<=i+i-1;l++){
			if(i<=(rows/2)+1){
				printf("* ");
			}else if(i>rows/2){
				printf("* ");
			}
		}
		printf("\n");
	}
}

