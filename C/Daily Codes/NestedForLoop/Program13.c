/*
 * 1 A 2 B
 * 1 A 2
 * 1 A
 * 1
 */

#include<stdio.h>
void main(){
	int rows, no, ch;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int i = rows; i>=1; i--){
		ch = 'A';
		no = 1;
		for(int j=1; j<=i; j++){
			if( j%2==0){
				printf("%c ", ch);
				ch++;
			}else{
				printf("%d ", no);
				no++;
			}
		}
		printf("\n");
	}
}
