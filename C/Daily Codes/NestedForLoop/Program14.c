/*
 *       *
 *     * *
 *   * * *
 * * * * *
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr = 1; ritr<=rows; ritr++){
		for(int sitr = rows -1; sitr>=ritr; sitr--){
			printf("  ");
		}
		for(int citr = 1; citr <= ritr; citr++){
			printf("* ");
		}
		printf("\n");
	}
}
