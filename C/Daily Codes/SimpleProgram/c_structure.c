#include<stdio.h>	//declaration of predefined functions

int a = 10;		//global variables

void fun()
{
	int x = 20; 	//local variable
	printf("In fun.\n");
}

void main()
{
	int y = 30; 	//local variable
	printf("Start main.\n");
	fun();
	printf("End main.\n");
}
