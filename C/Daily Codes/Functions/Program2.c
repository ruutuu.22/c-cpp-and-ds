

/*
 * returning the value of local variable
 */

#include<stdio.h>
int *fun(int x, int y){
	printf("%d\n", x+y);
	int val;
	val = x+y;
	return &val;
}

void main(){
	int *ptr = fun(10, 20);
	printf("%p\n", ptr);
	printf("%d\n", *ptr);	//segmentation fault

}
//Never return the value of local variable, if returned segmentation fault occurs
//Global variable value can be returned as it is placed in data section
