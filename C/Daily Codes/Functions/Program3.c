/*
 * returning an local array
 */


#include<stdio.h>
int *fun(){
	int arr[]={10,20,30};
	return arr;		//warning
}
void main(){
	int *ptr = fun();
	printf("%d\n", *ptr);	//segmentation fault
}
