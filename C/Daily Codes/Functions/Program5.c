/*
 * returning multipe values from functions
 */

#include<stdio.h>
void fun(int x, int y, int *ptradd, int *ptrsub, int *ptrmul){
	*ptradd = x+y;
	*ptrsub = x-y;
	*ptrmul = x*y;
}
void main(){
	int x = 10;
	int y = 20;

	int add;
	int sub;
	int mul;

	fun(10, 20, &add, &sub, &mul);

	printf("add : %d\n", add);
	printf("sub : %d\n", sub);
	printf("mul : %d\n", mul);
}
