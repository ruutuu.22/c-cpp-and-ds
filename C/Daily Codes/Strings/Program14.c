/*
 * mystrlen()  [not built in]
 */

#include<stdio.h>
int mystrlen(char *);		// prototype of strlen()
void main(){
	char name[10] = {'K', 'L','R', 'a', 'h', 'u','l', '\0'};
	char *pname = "Virat Kholi";

	int lenName = mystrlen(name);
	int lenPname = mystrlen(pname);

	printf("name = %d", lenName);

	printf("pname = %d", lenPname);
}

int mystrlen(char *str){
	int count = 0;
	while(*str!=0){
		count++;
		str++;
	}
	return count;
}
