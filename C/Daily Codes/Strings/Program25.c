/*
 * mystrlwr()
 * mystrupr()
 */

#include<stdio.h>
char *mystrlwr(char *str){
	while(*str != '\0'){
		if(*str>='A' && *str<='Z'){
			*str = *str + 32;
		}
		str++;
	}
	return str;
}
char *mystrupr(char *str){
	while(*str != '\0'){
		if(*str>='a' && *str<='z'){
			*str = *str - 32;
		}
		str++;
	}
	return str;
}
void main(){
	char arr[20] = "RuTuPaRn";
	puts(arr);

	mystrlwr(arr);
	puts(arr);

	mystrupr(arr);
	puts(arr);
}
