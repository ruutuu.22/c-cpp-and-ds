/*
 * strlen
 */

#include<stdio.h>
#include<string.h>		// warning if not included
void main(){
	char name[10] = {'K', 'L','R', 'a', 'h', 'u','l', '\0'};
	char *pname = "Virat Kholi";

	int lenName = strlen(name);
	int lenPname = strlen(pname);

	printf("name = %d", lenName);

	printf("pname = %d", lenPname);
}
