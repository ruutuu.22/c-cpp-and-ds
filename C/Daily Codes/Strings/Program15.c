/*
 * strcpy()
 */

#include<stdio.h>
#include<string.h>
void main(){
	char *arr1 = "Hardik Pandya";
	char arr2[20];

	puts(arr1);
	puts(arr2);

	strcpy(arr2, arr1);
	/*
	 * strcpy(dest, src);
	 * if,
	 * strcpy(src, dest);
	 * this gives segmentation fault as this tries to change the READ ONLY DATA.
	 */

	puts(arr1);
	puts(arr2);
}
