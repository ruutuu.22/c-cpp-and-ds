/*
 * mystrcmpi()
 */

#include<stdio.h>
#include<string.h>
int mystrcmpi(char *src, char *dest){
	while(*src != '\0'){
		if(*src == *dest || *src - *dest == -32 || *src - *dest == 32){
			src++;
			dest++;
		}else{
			return(*src-*dest);
		}
	}
	return 0;
}

void main(){
	char *arr1 = "Ashish";
	char *arr2 = "ashish";
	int diff = 1;
	if(strlen(arr1) == strlen(arr2)){
		diff = mystrcmpi(arr1, arr2);
	}
	if(diff == 0){
		printf("Strings are same.\n");
	}else{
		printf("Strings are not same.\n");
	}
}

