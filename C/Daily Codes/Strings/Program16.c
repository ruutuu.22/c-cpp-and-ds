/*
 * mystrcpy()
 */

#include<stdio.h>
char *mystrcpy(char *dest, const char *src);		//prototype of strcpy()
void main(){
	char *arr1 = "Hardik Pandya";
	char arr2[20];

	puts(arr1);
	puts(arr2);

	mystrcpy(arr2, arr1);

	puts(arr1);
	puts(arr2);
}

char *mystrcpy(char *dest, const char *src){
	while(*src != '\0'){
		*dest = *src;
		src++;
		dest++;
	}
	*dest = '\0';
}
