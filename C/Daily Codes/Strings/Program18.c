/*
 * strcat()
 */

#include<stdio.h>
#include<string.h>

void main(){
	char str1[20] = {'R', 'u', 't', 'u', 'p', 'a', 'r', 'n', '\0'};
	char *str2 = "Sadvelkar";

	puts(str1);
	puts(str2);

	strcat(str1, str2);

	puts(str1);
	puts(str2);
}

