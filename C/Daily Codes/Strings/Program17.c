/*
 * strcmp()
 */

#include<stdio.h>
#include<string.h>

void main(){
	char *arr1 = "Rutuparn";
	char *arr2 = "Rutu";
	char *arr3 = "Rutuparna";
	char *arr4 = "Rutuparr";

	int diff1 = strcmp(arr1, arr2);
	int diff2 = strcmp(arr1, arr3);
	int diff3 = strcmp(arr1, arr4);

	printf("Diff after comparing arr1 and arr2: %d.\n", diff1);
	printf("Diff after comparing arr1 and arr3: %d.\n", diff2);
	printf("Diff after comparing arr1 and arr4: %d.\n", diff3);
}





