/*
 * mystrcmp() : strings are equal or not
 */

#include<stdio.h>
int mystrlen(char *str){
	int count = 0;
	while(*str != '\0'){
		count++;
		str++;
	}
	return count;
}
int mystrcmp(char *str1, char *str2){
	while(*str1 == '\0'){
		if(*str1 == *str2){
			str1++;
			str2++;
		}else{
			return(*str1-*str2);
		}
	}
}

void main(){
	char *arr1 = "Rutuparn";
	char *arr2 = "rutuparn";
	int diff = 1;
	if(mystrlen(arr1) == mystrlen(arr2)){
		diff = mystrcmp(arr1, arr2);
	}
	if(diff == 0){
		printf("Strings are equal.\n");
	}else{
		printf("Strings are not equal.\n");
	}
}

