/*
 * mystrcmp()
 */

#include<stdio.h>
#include<string.h>

int mystrcmp(char *str1, char *str2);
void main(){
	char arr1[15] = "Rutu";
	char *arr2 = "Rutt";

	puts(arr1);
	puts(arr2);

	int diff=1;
	if(strlen(arr1) == strlen(arr2)){
		diff = mystrcmp(arr1, arr2); 
	}
	if(diff == 0){
		printf("Strings are same.\n");
	}else{
		printf("Strings are not same.\n");
	}
}

int mystrcmp(char *str1, char *str2){
	while(*str1 != '\0'){
		if(*str1 == *str2){
			str1++;
			str2++;
		}else{
			return(*str1 - *str2);
		}
	}
	return 0;
}

