

#include<stdio.h>
char *mystrcpy(char *dest, char *src, int num){
	int count = 0;
	while(*src != '\0' && num != 0){
		*dest = *src;
		dest++;
		src++;
		num--;
	}
	*dest = '\0';
}
void main(){
	char *arr1 = "Rutuparn Sadvelkar";
	char arr2[20];

	puts(arr1);
	puts(arr2);
	mystrcpy(arr2, arr1, 8);
	puts(arr1);
	puts(arr2);
}
