/*
 * Program10.c
 * Take a input number from the user and print the fibonacci series up to that number
 */

#include<stdio.h>
void main(){
	int number,fib, fib0 = 0, fib1 = 1;
	printf("Enter the number:\n");
	scanf("%d", &number);
	int i = 0;
	if(number > 0){
		fib = fib1+fib0;
		while(i <= number){
			fib1 = fib0;
			fib0 = fib;
			fib = fib1+fib0;
			if(fib1<=number){
				printf("%d\n", fib1);
			}
			i++;
		}
	}else{
		printf("Invalid Input.\n");
	}
}


