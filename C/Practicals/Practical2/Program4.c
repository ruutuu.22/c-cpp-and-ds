/*
 * Program4.c
 * WAP to take a num as input and print whether that is prime number or not
 */

#include<stdio.h>
void main(){
	int num, count = 0;
	printf("Enter the number: \n");
	scanf("%d", &num);
	
	if( num > 0){
		for(int i = 1 ; i <= num; i++){
			if(num%i == 0){
				count ++;
			}
		}
		if(count == 2){
			printf("%d is a prime number.\n", num);
		}else{
			printf("%d is not a prime number.\n", num);
		}
	}else{
		printf("Invalid Input.\n");
	}
}
