/*
 * Program9.c
 * Take a input number from the user and print the number in reverse
 */

#include<stdio.h>
void main(){
	int number, rem = 0, new = 0;
	printf("Enter the number:\n");
	scanf("%d", &number);
	if(number > 0){
		while(number != 0){
			rem = number%10;
			//printf("%d", rem);
			new = (new*10)+rem;
			number = number /10;
		}
		printf("%d\n",new );
	}else{
		printf("Invalid Input.\n");
	}
}


