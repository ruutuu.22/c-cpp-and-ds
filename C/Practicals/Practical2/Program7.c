/*
 * Program7.c
 * Take a input number from the user and print the sum of digits
 */

#include<stdio.h>
void main(){
	int number, rem = 0, sum= 0;
	printf("Enter the number:\n");
	scanf("%d", &number);
	if(number > 0){
		while(number != 0){
			rem = number%10;
			sum = sum + rem;
			number = number /10;
		}
		printf("Sum of digits is %d.\n", sum);
	}else{
		printf("Invalid Input.\n");
	}
}


