/*
 * Program1.c
 * WAP to find the sum of numbers that are not divisible by 3 upto a given number
 */

#include<stdio.h>
void main(){
	int num, sum = 0;
	printf("Enter number: \n");
	scanf("%d", &num);
	
	if(num>0){
		for(int i = 1; i<=num; i++){
			if(i%3!=0){
				sum = sum + i;
			}
		}
		printf("Sum of numbers not divisible by 3 = %d\n", sum);
	}else{
		printf("Input Invalid.\n");
	}
}
