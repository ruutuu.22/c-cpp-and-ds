/*
 * Program3.c
 * WAP to print the divisors and count of divisors ofthe entered number
 */

#include<stdio.h>
void main(){
	int num, count = 0, sum =0;
	printf("Enter the number: \n");
	scanf("%d", &num);

	if(num>0){
		printf("The divisors are: \n"); 
		for(int i = 1; i <= num; i++){
			if(num%i == 0){
				printf("%d\n", i);
				count++;
				sum = sum + i;
			}
		}
		printf("The Count of divisor = %d\n", count);
		printf("The Addition of divisor = %d\n", sum);
	}else{
		printf("Invalid Input.\n");
	}
}


