/*
 * Program5.c
 * WAP tom print all even numbers in reverse order and odd numbers in the standard way both separately.
 */

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start : \n");
	scanf("%d", &start);
	printf("Enter end : \n");
	scanf("%d", &end);

	if(start<end){
		for(int i=end; i>=start; i--){
			if(i%2==0){
				printf("%d\t", i);
			}
		}
		printf("\n");
		for(int i=start; i<=end; i++){
			if(i%2!=0){
				printf("%d\t", i);
			}
		}
		printf("\n");
	}else{
		for(int i=start; i>=end; i--){
			if(i%2==0){
				printf("%d\t", i);
			}
		}
		printf("\n");
		for(int i=end; i<=start; i++){
			if(i%2!=0){
				printf("%d\t", i);
			}
		}
		printf("\n");
	}
}

