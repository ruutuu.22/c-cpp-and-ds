/*
 * Program2.c
 * write a program to print the addition of 1 to 10 with 10 to 1
 */

#include<stdio.h>
void main(){
	int start, end, extra;
	printf("Enter Start: \n");
	scanf("%d", &start);
	printf("Enter End: \n");
	scanf("%d", &end);

	if(start < end){
		extra = end+1; 
		for(int i = start; i<=end; i++){
			extra = extra - 1;
			printf("%d + %d = %d\n", i, extra, i + extra);
		}
	}else{
		extra = start+1; 
		for(int i = end; i<=start; i++){
			extra = extra - 1; 
			printf("%d + %d = %d\n", i, extra, i + extra);
		}
	}
}


