/*
 * Program8.c
 * Take a input number from the user and print the product of digits
 */

#include<stdio.h>
void main(){
	int number, rem = 0, product = 1;
	printf("Enter the number:\n");
	scanf("%d", &number);
	if(number > 0){
		while(number != 0){
			rem = number%10;
			product = product * rem;
			number = number /10;
		}
		printf("Product of digits is %d.\n", product);
	}else{
		printf("Invalid Input.\n");
	}
}


