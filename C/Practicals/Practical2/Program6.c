/*
 * Program6.c
 * Take a input number from the user and count the no of digits
 */

#include<stdio.h>
void main(){
	int number, rem = 0, count = 0, num;
	printf("Enter the number:\n");
	scanf("%d", &number);
	num = number;
	while(num != 0){
		count ++;
		num = num /10;
	}
	printf("Number of digits in %d is %d.\n", number, count);
}


