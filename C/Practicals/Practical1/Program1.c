/*
 * Program1.c
 * WAP to check whether the input is a leap year or not(basic leap year)
 * input = 2000
 * output = leap year
 *
 * input = 1999
 * output = not a leap year
 */

#include<stdio.h>
void main()
{
	int year;
	printf("Enter year:\n");
	scanf("%d", &year);
	if(year >= 0){
		if(year%4==0 || year%400==0 && year%100!=0){
			printf("Leap Year.\n");
		}else{
			printf("Not a leap year.\n");
		}	
	}else{
		printf("Invalid Input.\n");
	}
}
