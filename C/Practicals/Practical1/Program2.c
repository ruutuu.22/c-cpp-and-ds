/*
 * Program2.c
 * WAP to find max among 3 numbers
 */

#include<stdio.h>
void main(){
	int a, b, c;
	printf("Enter the 1st number:\n");
	scanf("%d", &a);
	printf("Enter the 2nd number:\n");
        scanf("%d", &b);
	printf("Enter the 3rd number:\n"); 	
	scanf("%d", &c);
	
	if(a!=b && b!=c && a!=c){
		if(a>b && a>c){
				printf("%d is maximum.\n", a);	
		}else{
			if(b>c){
				printf("%d is maximum.\n", b);
			}else{
				printf("%d is maximum.\n", c);
			}	
		}
	}else{
		printf("Some of the entered numbers are equal.\n");
	}
}
