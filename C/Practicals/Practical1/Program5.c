/*
 * Program5.c
 * Write a program that takes a number from 0 t0 5 and prints its spelling.
 * if the number is greater than 5 print entered number is greater than 5
 */

#include<stdio.h>
void main(){
	int number;
	printf("Enter the number:\n");
	scanf("%d", &number);

	switch(number){
		case 0:
			printf("ZERO.\n");
			break;
		case 1:
			printf("ONE.\n");
			break;
		case 2:
			printf("TWO.\n");
			break;
		case 3:
			printf("THREE.\n");
			break;
		case 4:
			printf("FOUR.\n");
			break;
		case 5:
			printf("FIVE.\n");
			break;
		default:
			if(number > 5){
				printf("Entered number is greater than 5.\n");
			}else{
				printf("INVALID INPUT.\n");

			}
	}
}

