/*
 * Program7.c
 * WAP to check whether the given input is a pythagoreon triplet
 */

#include<stdio.h>
void main(){
	int a, b, c;
	printf("Enter value A = \n");
	scanf("%d", &a);
	printf("Enter value B = \n");
	scanf("%d", &b);
	printf("Enter value C = \n");
	scanf("%d", &c);

	if(a>=0 && b>=0 && c>=0){
		if((c*c) == (a*a)+(b*b) ||(c*c)+(a*a) == (b*b) || (a*a) == (b*b)+(c*c)){
			printf("%d, %d and %d are pythagorean triplet.\n", a, b, c);
		}else{
			printf("%d, %d and %d are not the pythagorean triplet.\n", a, b, c);
		}
	}else{
		printf("Invaild Input.\n");
	}
}

