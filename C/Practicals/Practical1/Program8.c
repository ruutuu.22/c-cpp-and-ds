/*
 * WAP that takes angles of a triangle from the user and print whether that triangle is valid or not
 */

#include<stdio.h>
void main(){
	int ang1, ang2, ang3;
	printf("Enter the measure of angle 1 = \n");
	scanf("%d", &ang1);
	printf("Enter the measure of angle 2 = \n");
       	scanf("%d", &ang2);	
	printf("Enter the measure of angle 3 = \n");
       	scanf("%d", &ang3);
	
	if(ang1>0 && ang1<180 && ang2>0 && ang2<180 && ang3>0 && ang3<180){
		if(ang1+ang2+ang3 == 180){
			printf("The Triangle with angles %d, %d and %d is valid.\n", ang1, ang2, ang3);
		}else{
			printf("The Triangle with angles %d, %d and %d is invalid.\n", ang1, ang2, ang3);
		}
	}else{
		printf("INPUT INVALID.\n");
	}
}	
