/*
 * Write a program, in which according to the month number print the no. of days in that month
 * (use switch case)
 *
 * input : month = 7
 * ouput : July has 31 days
 *
 */

#include<stdio.h>
void main(){
	int month_no;
	printf("Enter the month number:\n");
	scanf("%d", &month_no);

	printf("Month = %d\n", month_no);

	switch(month_no){
		case 1:
			printf("JAN has 31 days.\n");
			break;
		case 2:
			printf("FEB has 28/29 days.\n");
			break;
		case 3:
			printf("MAR has 31 days.\n");
		     	break;
		case 4:
			printf("APRIL has 30 days.\n");
			break;
		case 5:
			printf("MAY has 31 days.\n");
			break;
		case 6:
			printf("JUNE has 30 days.\n");
			break;
		case 7:
			printf("JULY has 31 days.\n");
			break;
		case 8:
			printf("AUG has 31 days.\n");
			break;
		case 9:
			printf("SEPT has 30 days.\n");
			break;
		case 10:
			printf("OCT has 31 days.\n");
			break;
		case 11:
			printf("NOV has 30 days.\n"); 
			break;		
		case 12:
			printf("DEC has 31 days.\n");
		        break;
		default:
			printf("Invalid input.\n");
	}
}	
