/*
 * Program10.c
 * Write a program, take two characters if these characters are equal then print them as it is
 * but if they are unequal then print their difference.
 * Input: va1=s var2=s
 * Output: va1=s var2=s
 * Input: va1=a var2=p
 * Output: The difference between a and p is 15
 */

#include<stdio.h>
void main(){
	char var1, var2;
	printf("Enter Var1:\n");
	scanf("%c", &var1);
	printf("Enter Var2:\n");
	scanf(" %c", &var2);
	printf("\n");
	printf("Var1 = %c\n", var1);
	printf("Var2 = %c\n", var2);
	printf("\n");
	if(var1 == var2){
		printf("Var1 = %c\n", var1);
		printf("Var2 = %c\n", var2);
	}else{
		if(var1>var2){
			printf("The difference between %c and %c is %d\n", var1, var2, var1 - var2);
		}else{
			printf("The difference between %c and %c is %d\n", var1, var2, var2 - var1); 
		}
	}
}
