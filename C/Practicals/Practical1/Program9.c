/*
 * Program9.c
 * WAP to print the count of divisors of the entered num.
 * Ip: 16
 * Op: Divisors of 16 are
 * 1
 * 2
 * 4
 * 8
 */

#include<stdio.h>
void main(){
	int number, count = 0;
	printf("Enter the number: \n");
	scanf("%d", &number);
	printf("\n");
	printf("Divisors of %d are\n", number);
	for(int i = 1; i< number; i++){
		if(number%i==0){
			printf("%d\n", i);
			count++;
		}
	}
	printf("Count = %d\n", count);
}
