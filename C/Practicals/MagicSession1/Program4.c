/*
 * Program4.c
 * take no of rows from the user
 * * - - -
 * - * - -
 * - - * -
 * - - - *
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter the number of rows: \n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			if(i==j){
				printf("* ");
			}else{
				printf("_ ");
			}
		}
		printf("\n");
	}
}
