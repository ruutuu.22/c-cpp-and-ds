/*
 * Program2.c
 * WAP to count digits in given no
 * Input: 94211
 * Output: digit count is 5
 */

#include<stdio.h>
void main(){
	int num, count = 0;
	printf("Enter number : \n");
	scanf("%d", &num);

	while(num!=0){
		num = num / 10;
		count ++;
	}
	printf("Digit Count is %d.\n", count);
}
