/*
 * WAP that accepts an array of length N from the use and calculate square of all even elements and cubes of all odd respectively with answer
 */

#include<stdio.h>

void main(){
	int size;
	printf("Enter size:\n");
	scanf("%d", &size);

	int arr[size];
	printf("Enter Elemsnts:\n");
	for(int i=0; i<size; i++){
		scanf("%d", &arr[i]);
	}

	for(int i=0; i<size; i++){
		if(arr[i]%2==0){
			arr[i] = arr[i]*arr[i];
		}else{
			arr[i] = arr[i]*arr[i]*arr[i];
		}
	}

	printf("Array after operation:\n");
	for(int i=0; i<size; i++){
		printf("%d\t", arr[i]);
	}
	printf("\n");
}
