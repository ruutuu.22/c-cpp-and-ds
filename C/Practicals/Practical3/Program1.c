/*
 * WAP that accepts a string from  the uers and prints thr lenght of the string.
 */

#include<stdio.h>

int mystrlen( char* src){
	int count = 0;
	while(*src != '\0'){
		count++;
		src++;
	}
	return count;
}

void main(){
	char *arr1[] = {""};
	printf("Enter string:\n");
	gets(arr1);

	printf("The Length of entered string is %d.\n", mystrlen(arr1));
}
