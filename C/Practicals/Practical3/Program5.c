/*
 * wap that accepts 2 strings from the user a concat the strings and print the concated string usr mystrcat()
 */

#include<stdio.h>
char *mystrcat(char* str1, char* str2){
	while(*str1 != '\0'){
		str1++;
	}
	*str1 = ' ';
	str1++;
	while(*str2 != '\0'){
		*str1 = *str2;
		str1++;
		str2++;
	}
	*str2 = '\0';
}

void main(){
	char arr1[40];
	char arr2[20];

	printf("Enter string 1: \n");
	gets(arr1);
	printf("Enter string 2: \n");
	gets(arr2);

	mystrcat(arr1, arr2);

	puts(arr1);
}
