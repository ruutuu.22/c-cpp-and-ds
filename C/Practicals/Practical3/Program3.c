/*
 * wap that accepts number from user seperate digits from that number and enter them in an array then sort  the array in descending order.
 */

#include<stdio.h>
void main(){
	int size;
	printf("Enter size of array: \n");
	scanf("%d", &size);
	long num;
	printf("Enter no.:\n");
	scanf("%ld", &num);
	
	int arr[size];
	int i = 0;
	int count = 0;
	while(num!=0){
		arr[i] = num % 10;
		num = num/10;
		i++;
		count++;
	}
	
	for(int i=0; i<count; i++){
		for(int j=0; j<count; j++){
			if(arr[i]>arr[j]){
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
				
	for(int i=0; i<count; i++){
		if(arr[i] != arr[i+1]){
			printf("%d|",arr[i]);
		}
	}
	printf("\n");
}

