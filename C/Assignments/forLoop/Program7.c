/*
 * Program7.c
 * WAP to calculate the LCM of given two numbers.
 */

#include<stdio.h>
void main(){
	int num1, num2, max_no;
	printf("Enter num1 : \n");
	scanf("%d", &num1);
	printf("Enter num2 : \n");
	scanf("%d", &num2);

	for(int i=1; i>0; i++){
		if (i % num1 == 0 && i % num2 == 0){
			printf("The LCM of %d and %d is %d.\n", num1, num2, i);
			break;
		}
	}
}
