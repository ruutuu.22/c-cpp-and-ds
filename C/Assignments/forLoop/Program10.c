/*
 * Program10.c
 * WAP to print the numbers in a given range and their multiplicative inverse.
 * Suppose x is a number then its multiplicative inverse or reciprocal is 1/x.
 * The expected output for range 1 - 5
 * 1 = 1
 * 2 = 1/2 i.e 0.5
 * 3 = 1/3 i.e 0.33
 * 4 = 0.25
 * 5 = 0.20
 */

#include<stdio.h>
void main(){
	int start, end;
	float new;
	printf("Enter start : \n");
	scanf("%d", &start);
	printf("Enter end : \n");
	scanf("%d", &end);
	if(start<end){
		for(int i=start; i<=end; i++){
			new = 1.0/i;
			printf("%d = %f\n", i, new);
		}
		printf("\n");
	}else{
		for(int i=end; i<=start; i++){
			 new = 1.0/i;               
		      	 printf("%d = %f\n", i, new);
		}
		printf("\n");
	}
}

