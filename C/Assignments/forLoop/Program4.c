/*
 * Program4.c
 * WAP to find the sum of numbers that are divisible by 5 in the given range.
 */

#include<stdio.h>
void main(){
	int start, end, add=0;
	printf("Enter start : \n");
	scanf("%d", &start);
	printf("Enter end : \n");
	scanf("%d", &end);
	printf("\n"); 
	if(start<end){
		for(int i=end; i>=start; i--){
			if(i%5==0){
				add = add + i;
			}
		}
		printf("Sum of numbers that are divisible by 5 in given range is %d.\n", add);
	}else{
		for(int i=start; i>=end; i--){
			if(i%5==0){
				add = add + i;
			}
		}
		printf("Sum of numbers that are divisible by 5 in given range is %d.\n", add);
	}
}

