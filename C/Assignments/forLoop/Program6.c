/*
 * Program6.c
 * WAP to calculate the factorial of a given number.
 */

#include<stdio.h>
void main(){
	int num, fact = 1;
	printf("Enter the number : \n");
	scanf("%d", &num);
	printf("\n");

	if(num > 0){
		printf("Factors of %d are:\n", num);
		for(int i=1; i <= num; i++){
			fact = fact*i;
		}
		printf("Factorial of %d is %d\n", num, fact);
	}else{
		printf("Invalid input.\n");
	}
}
