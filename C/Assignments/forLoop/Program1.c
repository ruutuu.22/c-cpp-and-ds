/*
 * Program1.c
 * WAP a program to see a given number is a multiple of 3
 */

#include<stdio.h>
void main(){
	int num;
	printf("Enter the number : \n");
	scanf("%d", &num);

	if(num > 0){
		if(num%3==0){
			printf("%d is divisible by 3.\n", num);
		}else{
			printf("%d is not divisible by 3.\n", num);
		}
	}else{
		printf("Invalid Input.\n");
	}
}
