/*
 * Program5.c
 * WAP to take the number input and print all the factors of that number.
 */

#include<stdio.h>
void main(){
	int num;
	printf("Enter the number : \n");
	scanf("%d", &num);
	printf("\n");
	if(num > 0){
		printf("Factors of %d are:\n", num);
		for(int i=1; i <= num; i++){
			if(num%i==0){
				printf("%d\t", i);
			}
		}
		printf("\n");
	}else{
		printf("Invalid input.\n");
	}
}
