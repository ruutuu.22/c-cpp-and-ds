/*
 * Program8.c
 * WAP to take two characters if these characters are equal then equal then print the same but if they are unequal then print their difference.
 */


#include<stdio.h>
void main(){
	char var1, var2;
	printf("Enter Var1:\n");
	scanf("%c", &var1);
	printf("Enter Var2:\n");
	scanf(" %c", &var2);
	printf("\n");
	printf("Var1 = %c\n", var1);
	printf("Var2 = %c\n", var2);
	printf("\n");
	if(var1 == var2){
		printf("Var1 = %c\n", var1);
		printf("Var2 = %c\n", var2);
	}else{
		if(var1>var2){
			printf("The difference between %c and %c is %d\n", var1, var2, var1 - var2);
		}else{
			printf("The difference between %c and %c is %d\n", var1, var2, var2 - var1); 
		}
	}
}
