/*
 * Program9.c
 * WAP to calculate the square root of a number ranging from 100 to 300
 */

#include<stdio.h>
void main(){
	int num;
	printf("Enter the number to find its square root : \n");
	scanf("%d", &num);
	int p_square;
	int p_square_root;
	float sq_root;
	for(int i = 1; i*i <= num; i++){
		if((i*i)%i == 0 && (i*i)>=(num*9)/10 && (i*i)<=num){
		       p_square = i*i;
		       p_square_root = i;
		       sq_root = (num + p_square)/(2.0*p_square_root);
		       printf("Square Root of %d is %f\n",num,sq_root);

		       printf("i = %d", i);
		       printf("i = %d", i*i);
		       break;
	       }
	}	       
}
