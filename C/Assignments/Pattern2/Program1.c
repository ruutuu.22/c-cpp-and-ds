/*
 * Program1.c
 * take no of rows from the user
 * 4 3 2 1
 * 5 4 3 2
 * 6 5 4 3
 * 7 6 5 4
 */

#include<stdio.h>
void main(){
	int rows, x;
	printf("Enter the number of rows = \n");
	scanf("%d", &rows);
	
	printf("\n");
	for(int i = 1; i<=rows; i++){
		x = rows+i-1;
		for(int j = rows; j>=1; j--){
			printf("%d\t", x);
			x--;
		}

		printf("\n");
	}
}


