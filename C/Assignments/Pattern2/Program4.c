/*
 * Program4.c
 * take no of rows from the user
 * I H G
 * F E D
 * C B A
 */

#include<stdio.h>
void main(){
	int rows;
	char x ;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	x = 'A'+(rows*rows)-1;

	for(int i=rows; i>=1; i--){
		for(int j=rows; j>=1; j--){
			printf("%c\t", x);
			x--;
		}
		printf("\n");
	}
}

