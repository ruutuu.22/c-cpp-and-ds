/*
 *  Program9.c
 *  take no of rows from the user
 *  1  3  8
 *  15 24 35
 *  48 63 80
 */


#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);

	for(int i=1; i <= rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d\t", ((x*x)-1));
			x++;
		}
		printf("\n");
	}
}
