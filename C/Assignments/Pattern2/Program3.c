/*
 * Program3.c
 * take no of rows from the user
 * 4 4 4 4
 * 3 3 3 3
 * 2 2 2 2
 * 1 1 1 1
 */

#include<stdio.h>
void main(){
	int rows, x;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	x = rows;
	for(int i=rows; i>=1; i--){
		for(int j=1; j<=rows; j++){
			printf("%d\t", i);
		}
		printf("\n");
	}
}


