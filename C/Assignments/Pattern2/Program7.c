/*
 * Program7.c
 * ake no of rows from the user
 * 1   2   3   4
 * 25  36  49  64
 * 9   10  11  12
 * 169 196 225 256
 */

#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);
	printf("\n");
	for(int i=1; i <= rows; i++){
		if(i%2==0){
			for(int j=1; j<=rows; j++){
				printf("%d\t", x*x);
				x++;
			}
		}else{
			for(int j=1; j<=rows; j++){ 
				printf("%d\t ", x);
				x++;
			}
		}
		printf("\n");
	}
}
