/*
 * Program2.c
 * take no of rows from the user
 * 3 2 1
 * c b a
 * 3 2 1
 * c b a
 */

#include<stdio.h>
void main(){
	int rows, columns;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	printf("Enter the number of columns : \n");
	scanf("%d", &columns);
	printf("\n");
	for(int i=1; i<=rows; i++){
		char x = 'a'+columns-1; 
		if(i%2==0){ 
			for(int j=columns; j>=1; j--){
				printf("%c\t",x);
				x--;
			}
		}else{
			for(int j=columns; j>=1; j--){
				printf("%d\t",j);
			}
		}
		printf("\n");
	}
}


