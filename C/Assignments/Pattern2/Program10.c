/*
 * Program10.c
 * take no of rows from the user
 * D4 C3 B2 A1
 * A1 B2 C3 D4
 * D4 C3 B2 A1
 * A1 B2 C3 D4
 */

#include<stdio.h>
void main(){
	int rows;
	char x;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	printf("\n");


	for(int i=1; i <= rows; i++){
		if(i%2==0){
			x = 'A';
			for(int j=1; j<=rows;j++){        
			      	printf("%c%d ",x,j);   
			   	x++;
			}
		}else{		
			x = 'A' + rows - 1;
			for(int j=rows; j>=1;j--){
				printf("%c%d ",x,j);
				x--;
			}
		}
		printf("\n");
	}
}

