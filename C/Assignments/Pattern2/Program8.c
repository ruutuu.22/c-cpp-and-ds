/*
 * Program8.c
 * take no of rows from the user
 * 18 16 14
 * 12 10 8
 * 6  4  2
 */

#include<stdio.h>
void main(){
	int rows, x = 1;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	x = 2 *(rows*rows);

	for(int i=rows; i>=1; i--){
		for(int j=rows; j>=1; j--){
			printf("%d\t", x);
			x=x-2;
		}
		printf("\n");
	}
}

