/*
 * Program5.c
 * take no of rows from the user
 * D C B A
 * e d c b
 * F E D C
 * g f e d
 */

#include<stdio.h>
void main(){
	int rows, columns;
	char x;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	
	printf("\n");
	for(int i=1; i<=rows; i++){
		if(i%2==0){ 
			x = 'a'+rows+i-2;
			for(int j=rows; j>=1; j--){
				printf("%c\t",x);
				x--;
			}
		}else{
			x = 'A'+rows+i-2;
			for(int j=rows; j>=1; j--){
				printf("%c\t",x);
				x--;
			}
		}
		printf("\n");
	}
}


