/*
 * Program6.c
 * ake no of rows from the user
 * = = = =
 * $ $ $ $
 * = = = =
 * $ $ $ $
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);
	for(int i=1; i <= rows; i++){
		if(i%2==0){
			for(int j=1; j<=rows; j++){
				printf("$\t");
			}
		}else{
			for(int j=1; j<=rows; j++){ 
				printf("=\t");
			}
		}
		printf("\n");
	}
}
