/*
 *  Program5.c
 *  If possible take no of rows from the user
 *  A B C D
 *  B C D E
 *  C D E F
 *  D E F G
 */


#include<stdio.h>
void main(){
	int rows, y = 0;
       	char x;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);

	for(int i=1; i <= rows; i++){
		x='A' + y;
		for(int j=1; j<=rows; j++){
			printf("%c ", x);
			x++;
		}
		y++;
		printf("\n");
	}
}
