/*
 *  Program9.c
 *  If possible take no of rows from the user
 *  2  5  10
 *  17 26 37
 *  50 65 82
 */


#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);

	for(int i=1; i <= rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d\t", ((x*x)+1));
			x++;
		}
		printf("\n");
	}
}
