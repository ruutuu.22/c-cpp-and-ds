/*
 * Program10.c
 * If possible take no of rows from the user
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 * D4 C3 B2 A1
 */

#include<stdio.h>
void main(){
	int rows;
	char x;
	printf("Enter the number of rows : \n");
	scanf("%d", &rows);
	printf("\n");


	for(int i=1; i <= rows; i++){
		x = 'A' + rows - 1;
		for(int j=rows; j>=1;j--){
			printf("%c%d ",x,j);
			x--;
		}
		printf("\n");
	}
}

