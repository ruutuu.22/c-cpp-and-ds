/*
 *  Program8.c
 *  If possible take no of rows from the user
 *  1  3  5
 *  7  9  11
 *  13 15 17
 */


#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);

	for(int i=1; i <= rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d\t", x);
			x=x+2;
		}
		printf("\n");
	}
}
