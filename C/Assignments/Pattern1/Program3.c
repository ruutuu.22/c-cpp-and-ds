/*
 *  Program3.c
 *  If possible take no of rows from the user
 *  1 1 1 1
 *  2 2 2 2
 *  3 3 3 3
 *  4 4 4 4
 */


#include<stdio.h>
void main(){
	int rows, x;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);

	for(int i=1; i <= rows; i++){
		x=i;
		for(int j=1; j<=rows; j++){
			printf("%d ", x);
		}
		x++;
		printf("\n");
	}
}
