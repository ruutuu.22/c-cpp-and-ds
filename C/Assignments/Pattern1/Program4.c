/*
 *  Program4.c
 *  If possible take no of rows from the user
 *  A B C
 *  D E F
 *  G H I
 */


#include<stdio.h>
void main(){
	int rows;
       	char x='A';
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);

	for(int i=1; i <= rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%c ", x);
			x++;
		}
		printf("\n");
	}
}
