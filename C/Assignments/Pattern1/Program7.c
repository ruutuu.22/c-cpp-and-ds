/*
 *  Program7.c
 *  If possible take no of rows from the user
 *  1   2  9   4
 *  25  6  49  8
 *  81  10 121 12
 *  169 14 225 16
 */


#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);
	printf("\n");

	for(int i=1; i <= rows; i++){
		for(int j=1; j<=rows; j++){
			if(j%2==1){
				printf("%d\t", x*x);
			}else{
				printf("%d\t ", x);
			}
			x++;
		}
		printf("\n");
	}
}
