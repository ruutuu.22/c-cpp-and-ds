/*
 *  Program2.c
 *  If possible take no of rows from the user
 *  1 2 3
 *  a b c
 *  1 2 3
 *  a b c
 */


#include<stdio.h>
void main(){
	int rows, columns;
	char x;
	printf("Enter the number of Rows : \n");
	scanf("%d", &rows);
	printf("Enter the number of Columns : \n");
	scanf("%d", &columns);                     

	for(int i=1; i <= rows; i++){
		x='a'; 
		if(i%2==0){
			for(int j=1; j<= columns; j++){
				printf("%c ", x);
				x++;
			}
		}else{
			for(int j=1; j<= columns; j++){
				printf("%d ", j);
			}
		}			
		printf("\n");
	}
}
