/*
 * Program5.c
 * Write a program to print ASCII values from 0 to 127
 */

#include<stdio.h>
void main(){
	int number1, number2;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	printf("\n");
	for(int i = number1; i<= number2 ; i++){
		printf("%c = %d\n", i, i);
	}
}
