/*
 * Program10.c
 * Write a program to print the product of the first 10 numbers
 */
#include<stdio.h>
void main(){
	int number1, number2, product = 1;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	
	for(int i = number1; i <= number2; i++){
			product = product * i ;
	}
	printf("Product = %d\n", product);
}
