/*
 * Program2.c
 * Write a program to print the first 100 numbers
 */
#include<stdio.h>
void main(){
	int number1, number2;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	
	for(int i = number1; i <= number2; i++){
		printf("%d\n", i);
	}
	
}
