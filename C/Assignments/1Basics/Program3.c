/*
 * Program3.c
 * Write a program to print the first ten, 3 digit number.
 */

#include<stdio.h>
void main(){
	int number1, number2;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	printf("\n");
	if(number1>=100 && number1<=999 && number2>= 100 && number2 <=999){
		for(int i = number1; i<= number2 ; i++){
			if(i >= 100 && i <= 999){
				printf("%d\n", i);
			}
		}
	}else{
		printf("Invalid Input.\n");
	}
}

