/*
 * Program9.c
 * Write a program to print the sum of the first 10 odd numbers
 */
#include<stdio.h>
void main(){
	int number1, number2, sum = 0;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	
	for(int i = number1; i <= number2; i++){
		if(i%2 == 1){
			sum = sum + i ;
		}
	}
	printf("Sum = %d\n", sum);
}
