/*
 * Program4.c
 * Write a program to print even numbers 1- 100
 */

#include<stdio.h>
void main(){
	int number1, number2;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	printf("\n");
	for(int i = number1; i<= number2 ; i++){
		if(i%2==0){
			printf("%d\n", i);
		}
	}
}
