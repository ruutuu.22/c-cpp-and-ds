/*
 * Program6.c
 * Write a program to print reverse numbers 100-1
 */

#include<stdio.h>
void main(){
	int number1, number2;
	printf("Enter start:\n");
	scanf("%d", &number1);
	printf("Enter end:\n");
	scanf("%d", &number2);
	printf("\n");
	if(number2 > number1){
		
		for(int i = number2; i>= number1 ; i--){
				printf("%d\n", i);
		}
	}
}
