/*
 * Program7.c
 * Write a program to print a table of 12
 */

#include<stdio.h>
void main(){
	int number, table;
	printf("Enter the number:\n");
	scanf("%d", &number);

	for(int i = 1; i <= 10; i++){
		table = number * i;
		printf("%d\n", table);
	}
}
