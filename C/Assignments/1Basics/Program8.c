/*
 * Program8.c
 * Write a program to print a table of 11 in reverse order
 */

#include<stdio.h>
void main(){
	int number, table;
	printf("Enter the number:\n");
	scanf("%d", &number);

	for(int i = 10; i >= 1; i--){
		table = number * i;
		printf("%d\n", table);
	}
}
