/*
 * Program4.c
 * ake no of rows from the user
 * a B c D
 * b C d E
 * c D e F
 * d E f G
 */

#include<stdio.h>
void main(){
	int rows;
	char ch_cap='A', ch_sma='a';
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr=1; ritr<=rows; ritr++){
		for(int citr=1; citr<=rows; citr++){

			if(citr%2==0){
				printf("%c ", ch_cap);
			}else{
				printf("%c ", ch_sma);
			}
			ch_cap++;
			ch_sma++;
		}
		ch_cap = 'A' + ritr;
		ch_sma = 'a' + ritr ;
		printf("\n");
	}
}
				

