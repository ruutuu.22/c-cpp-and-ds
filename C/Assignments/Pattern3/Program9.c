/*
 * Program9.c
 * 0   1   1   2
 * 3   5   8   13
 * 21  34  55  89
 * 144 233 377 610
 */

#include<stdio.h>
void main(){
	int rows, no1=0, no2=1, new_no;
	printf("Enter rows : \n");
	scanf("%d", &rows);
	new_no = no1 + no2;
	for(int ritr=1; ritr<=rows; ritr++){
		for(int i=1; i<=rows; i++){
			no2 = no1;
			no1 = new_no;
			new_no = no1 + no2;
			printf("%d\t", no2);
		}
		printf("\n");
	}
}

