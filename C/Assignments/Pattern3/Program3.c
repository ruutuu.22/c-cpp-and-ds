/*
 * Program1.c
 * take no of rows from the user
 * 4 a 3 b
 * 4 a 3 b
 * 4 a 3 b
 * 4 a 3 b
 */

#include<stdio.h>
void main(){
	int rows, no;
	char ch;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr=1; ritr<=rows; ritr++){
		ch = 'a';
		no = rows;
		for(int citr=1; citr<=rows; citr++){
			if(citr%2==0){
				printf("%c ", ch);
				ch++;
			}else{
				printf("%d ",no);
				no--;
			}
		}
		printf("\n");
	}
}

