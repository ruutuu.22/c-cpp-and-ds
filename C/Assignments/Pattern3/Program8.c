/*
 * Program8.c
 * take no of rows from the user
 * 16 15 14 13
 * L  K  J  I
 * 8  7  6  5
 * D  C  B  A
 */

#include<stdio.h>
void main(){
	int rows, no;
	char ch;

	printf("Enter rows : \n");
	scanf("%d", &rows);
	no = rows*rows;
	ch = 'A' + (rows*rows) - 1;
	for(int ritr=1; ritr<=rows; ritr++){
		if(ritr%2==0){
			for(int citr=1; citr<=rows; citr++){
				printf("%c\t", ch);
				ch--;
				no--;
			}
		}else{
			for(int citr=1; citr<=rows; citr++){
				printf("%d\t", no);
				ch--;
				no--;
			}
		}
		printf("\n");
	}
}


