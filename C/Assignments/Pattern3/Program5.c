/*
 * Program5.c
 * take no of rows from the user
 * 1  4  3
 * 16 5  36
 * 7  64 9
 */

#include<stdio.h>
void main(){
	int rows, x=1;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr=1; ritr<=rows; ritr++){
		if(ritr%2==0){
			for(int citr=1; citr<=rows; citr++){
				if(citr%2==0){
					printf("%d\t", x);
				}else{
					printf("%d\t", x*x);
				}
				x++;
			}
		}else{
			for(int citr=1; citr<=rows; citr++){
				if(citr%2!=0){
					printf("%d\t", x);
				}else{
					printf("%d\t", x*x);
				}
				x++;
			}
		}
		printf("\n");
	}
}
