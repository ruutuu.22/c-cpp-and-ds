/*
 * Program1.c
 * take no of rows from the user
 * 1 2 3 4
 * 1 3 5 7
 * 1 4 7 10
 * 1 5 9 13
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr=1; ritr<=rows; ritr++){
		int x = 1;
		for(int citr=1; citr<=rows; citr++){
			printf("%d\t", x);
			x=x+ritr;
		}
		printf("\n");
	}
}

