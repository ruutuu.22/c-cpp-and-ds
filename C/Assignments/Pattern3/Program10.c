/*
 * Program10.c
 * take no of rows from the user
 * D1 C2 B3 A4
 * e5 f4 g3 h2
 * F3 E4 D5 C6
 * g7 h6 i5 j4
 */

#include<stdio.h>
void main(){
	int rows, no;
	char ch_cap, ch_sma;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	no = 1;
	ch_cap = 'A';
	ch_sma = 'a' + rows;

	for(int ritr=1; ritr<=rows; ritr++){
		ch_cap = ch_cap + rows - 1;
		if(ritr%2==0){
			for(int citr=1; citr<=rows; citr++){
				printf("%c%d ", ch_sma, no);
				ch_sma++;
				ch_cap--;
				no--;
			}
			ch_sma = ch_sma-2;
			no = no + 2;
		}else{
			for(int citr=1; citr<=rows; citr++){
				printf("%c%d ", ch_cap, no);
				ch_cap--;
				no++;
			}
		}
		ch_cap = ch_cap+2;
		printf("\n");
	}
}


