/*
 * Program7.c
 * take no of rows from the user
 * 1  4  27
 * 4  27 16
 * 27 16 125
 */

#include<stdio.h>
void main(){
	int rows, x;
	printf("Enter rows : \n");
	scanf("%d", &rows);

	for(int ritr=1; ritr<=rows; ritr++){
		x = ritr;
		if(ritr%2==0){
			for(int citr=1; citr<=rows; citr++){
				if(citr%2==0){
					printf("%d\t", x*x*x);
				}else{
					printf("%d\t", x*x);
				}
				x++;
			}
		}else{
			for(int citr=1; citr<=rows; citr++){
				if(citr%2!=0){
					printf("%d\t", x*x*x);
				}else{
					printf("%d\t", x*x);
				}
				x++;
			}
		}
		printf("\n");
	}
}
