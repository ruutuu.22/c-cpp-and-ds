/*
 * Promgram6.c
 * take no of rows from the user
 * = = = = = =
 * $ $ $ $ $ $
 * @ @ @ @ @ @
 * = = = = = =
 * $ $ $ $ $ $
 * @ @ @ @ @ @
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows : \n");
	scanf("%d", &rows);
	for(int ritr=1; ritr<=rows; ritr++){
		for(int citr=1; citr<=rows; citr++){
			if(ritr%3==0){
				printf("@ ");
			}else if(ritr%3==1){
				printf("= ");
			}else if(ritr%3==2){
				printf("$ ");
			}
		}
		printf("\n");	
	}
}
