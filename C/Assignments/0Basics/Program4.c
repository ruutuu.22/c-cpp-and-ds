/*
 * Program4.c
 * WAP to find min among 2 numbers. Take all the values from the user.
 * input : 2 3
 * output : 2
 */

#include<stdio.h>
void main(){
	int number1, number2;

	printf("Enter the 1st no.:\n");
	scanf("%d", &number1);

	printf("Enter the 2nd no.:\n");
	scanf("%d", &number2);

	if(number1 < number2){
		printf("\n%d\n", number1);
	}else if(number1 > number2){
		printf("\n%d\n", number2);
	}else{
		printf("Both are same.\n");
	}
}
