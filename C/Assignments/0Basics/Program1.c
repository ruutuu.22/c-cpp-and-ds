/*
 * Program1,c
 * Write a program to print the value and size of the below variables.Take all
 * the values from user
 * num = 10
 * chr = 'S'
 * rs = 55.20
 * crMoney = 542154313480.544543  
 */


#include<stdio.h>
void main(){
	int num;
	char chr;
	float rs;
	long double crMoney;

	printf("Enter the value of Num:\n");
	scanf("%d", &num);

	printf("Enter the value of chr:\n");
	scanf(" %c", &chr);

	printf("Enter the value of rs:\n");
	scanf("%f", &rs);

	printf("Enter the value of crMoney:\n");
	scanf("%Lf", &crMoney);

	printf("Values:\n");
	printf("num = %d\n", num);
	printf("chr = %c\n", chr);
	printf("rs = %f\n", rs);
	printf("crMoney = %Lf\n", crMoney);

	printf("\nSize:\n");
	printf("num = %ld\n", sizeof(num));
	printf("num = %ld\n", sizeof(chr));
	printf("num = %ld\n", sizeof(rs));
	printf("num = %ld\n", sizeof(crMoney));
}
