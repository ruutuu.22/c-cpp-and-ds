/*
 * Program5.c
 * WAP to take numerical input from the user and find whether the number is
 * divisible by 5 and 11. Take all the values from the user.
 * input: 55
 * output : 55 is divisible by 5 & 11
 *
 * input: 15
 * output: 15 is not divisible by 5 & 11
 */

#include<stdio.h>
void main(){
	int number;
	printf("Enter value:\n");
	scanf("%d", &number);

	if(number%5 == 0 && number%11 == 0){
		printf("\n%d is divisible by 5 & 11.\n", number);
	}else{
		printf("\n%d is not divisible by 5 & 11.\n", number);
	}
}

