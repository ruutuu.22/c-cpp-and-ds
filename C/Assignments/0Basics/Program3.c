/*
 * Program3.c
 * WAP to find max among 2 numbers. Take all vakues from the user.
 * input : 2 4
 * output : 4
 */

#include<stdio.h>
void main(){
	int number1, number2;

	printf("Enter the 1st no.:\n");
	scanf("%d", &number1);

	printf("Enter the 2nd no.:\n");
	scanf("%d", &number2);

	if(number1 > number2){
		printf("\n%d\n", number1);
	}else if(number1 < number2){
		printf("\n%d\n", number2);
	}else{
		printf("Both are same.\n");
	}
}
