/*
 * 	d d d d
 * 	  c c c 
 * 	    b b
 * 	      a
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		char ch = 'a' + rows - i;
		for(int sp=1; sp<=i-1; sp++){
			printf(" \t");
		}
		for(int j=rows; j>=i; j--){
			printf("%c\t", ch);
		}
		printf("\n");
	}
}
