/*
 * 	1 2 3 4
 * 	  5 6 7
 * 	    8 9 
 * 	      10
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	int x = 1;
	for(int i=1; i<=rows; i++){
		for(int sp=1; sp<=i-1; sp++){
			printf(" \t");
		}
		for(int j=rows; j>=i; j--){
			printf("%d\t", x);
			x++;
		}
		printf("\n");
	}
}
