/*
 * 	1 2 3 4
 * 	  1 2 3
 * 	    1 2 
 * 	      1
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		int x = 1;
		for(int sp=1; sp<=i-1; sp++){
			printf(" \t");
		}
		for(int j=rows; j>=i; j--){
			printf("%d\t", x);
			x++;
		}
		printf("\n");
	}
}
