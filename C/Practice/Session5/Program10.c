/*
 * 	a B c D
 * 	  e F g 
 * 	    H i
 * 	      J
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	int x = 1;
	char ch_s = 'a';
	char ch_c = 'A';

	for(int i=1; i<=rows; i++){
		for(int sp=1; sp<=i-1; sp++){
			printf("  ");
		}
		for(int j=rows; j>=i; j--){
			if(x%2!=0){
				printf("%c ", ch_s);
				ch_s++;
				ch_c++;
				x++;
			}else{
				printf("%c ", ch_c);
				ch_s++;
				ch_c++;
				x++;
			}
		}
		printf("\n");
	}
}
