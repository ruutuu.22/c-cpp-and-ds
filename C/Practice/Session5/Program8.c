/*
 * 	D D D D 
 * 	  c c c 
 * 	    B B 
 * 	      a
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		char ch_s = 'a' + rows - i;
		char ch_c = 'A' + rows - i;
		for(int sp=1; sp<=i-1; sp++){
			printf(" \t");
		}
		for(int j=rows; j>=i; j--){
			if(i%2!=0){
				printf("%c\t", ch_c);
			}else{
				printf("%c\t", ch_s);
			}
		}
		printf("\n");
	}
}
