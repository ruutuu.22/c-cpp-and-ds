/*
 * 	* * * *
 * 	  * * *
 * 	    * *
 * 	      *
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		for(int sp=1; sp<=i-1; sp++){
			printf("  ");
		}
		for(int j=rows; j>=i; j--){
			printf("* ");
		}
		printf("\n");
	}
}
