
/*
 *WAP to print the numbers whose Factorial is even.Take range from user.
 */

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start: \n");
	scanf("%d", &start);
	printf("Enter end: \n");
	scanf("%d", &end);
	printf("Numbers whose factorial is even are:\n");
	for(int i = start; i<=end; i++){
		int fact = 1;
		for(int j=1; j<=i; j++){
			fact = fact*j;
		}
		if(fact%2==0){
			printf("%d\n", i);
		}
	}
}
