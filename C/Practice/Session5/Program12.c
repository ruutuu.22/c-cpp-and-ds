/*
 * 	A b C d
 * 	  e G i 
 * 	    K n
 * 	      q
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	char ch_s = 'a';
	char ch_c = 'A';

	for(int i=1; i<=rows; i++){
		for(int sp=1; sp<=i-1; sp++){
			printf("  ");
		}
		for(int j=rows; j>=i; j--){
			if(i%2!=0){
				if(j%2!=0){
					printf("%c ", ch_s);
				}else{
					printf("%c ", ch_c);
				}
				ch_s+=i;
				ch_c+=i;
			}else{
				if(j%2!=0){
					printf("%c ", ch_c);
				}else{
					printf("%c ", ch_s);
				}
				ch_s+=i;
				ch_c+=i;
			}
		}
		printf("\n");
	}
}
