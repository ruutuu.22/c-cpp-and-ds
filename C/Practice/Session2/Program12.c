/*
 * A b C
 * d E f
 * G h I
 */

#include<stdio.h>
void main(){
	int row;
	printf("Enter row:\n");
	scanf("%d", &row);

	printf("\n");

	char ch_small = 'a';
	char ch_capital = 'A';
	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){
			if(i%2!=0){
				if(j%2!=0){
					printf("%c ", ch_capital);
				}else{
					printf("%c ", ch_small);
				}
			}else{
				if(j%2==0){
					printf("%c ", ch_capital);
				}else{
					printf("%c ", ch_small);
				}
			}
			ch_small++;
			ch_capital++;
		}
		printf("\n");
	}
}
