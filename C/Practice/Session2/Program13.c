/*
 * 1 3 5 
 * 5 7 9 
 * 9 11 13
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");
	int x = 1;
	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d\t", x);
			x+=2;
		}
		printf("\n");
		x-=2;
	}
}

