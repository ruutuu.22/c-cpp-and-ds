/*
 * 10
 * I H
 * 7 6 5
 * D C B A
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows;\n");
	scanf("%d", &rows);

	printf("\n");

	int x = (rows*(rows+1))/2;
	char ch = 'A' + x - 1;

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=i; j++){
			if(i%2!=0){
				printf("%d\t", x);
			}else{
				printf("%c\t",ch);
			}
			ch--;
			x--;
		}
		printf("\n");
	}


}
