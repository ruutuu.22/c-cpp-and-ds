/*
 * 1 2 3 4 
 * a b c d 
 * # # # #
 * 5 6 7 8
 * e f g h
 * # # # #
 */


#include<stdio.h>
void main(){
	int rows, col;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("Enter columns: \n");
	scanf("%d", &col);

	printf("\n");
	
	int x = 1;
	char ch = 'a';

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=col; j++){
			if(i%3==1){
				printf("%d\t", x);
				x++;
			}else if(i%3==0){
				printf("#\t");
			}else{
				printf("%c\t", ch);
				ch++;
			}
		}
		printf("\n");
	}
}

