/*
 * 1 
 * 2 2 
 * 3 3 3
 * 4 4 4 4
 */

#include<stdio.h>
void main(){
	int row;
	printf("Enter row:\n");
	scanf("%d", &row);
	
	printf("\n");

	for(int i=1; i<=row; i++){
		for(int j=1; j<=i; j++){
			printf("%d\t", i);
		}
		printf("\n");
	}
}
