/*
 * 1 2 3
 * 2 3 4
 * 4 5 6
 */


#include<stdio.h>
void main(){
	int row;
	printf("Enter rows: \n");
	scanf("%d", &row);
	
	for(int i=1; i<=row; i++){
		int x = 1 + i - 1;
		for(int j=1; j<=row; j++){
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}
}
