/*
 * 4
 * 3 3
 * 2 2 2
 * 1 1 1 1
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		int x = 1 + rows - i;
		for(int j=1; j<=i; j++){
			printf("%d ", x);
		}
		printf("\n");
	}
}

