/*
 * D D D D
 * C C C C
 * B B B B
 * A A A A
 */

#include<stdio.h>
void main(){
	int row;
	printf("Enter rows: \n");
	scanf("%d", &row);
	printf("\n");

	char ch = 'A'+row-1;

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){
			printf("%c ", ch);
		}
		ch--;
		printf("\n");
	}
}

