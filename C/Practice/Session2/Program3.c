/*
 * A
 * B B 
 * C C C
 * D D D D
 */

#include<stdio.h>
void main(){
	int row;
	printf("Enter row:\n");
	scanf("%d", &row);
	char ch = 'A';
	printf("\n");

	for(int i=1; i<=row; i++){
		for(int j=1; j<=i; j++){
			printf("%c\t", ch);
		}
		ch++;
		printf("\n");
	}
}
