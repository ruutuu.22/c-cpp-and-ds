/*
 * wap to print the odd no as it is and cube of even no between given range
 */

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start:\n");
	scanf("%d", &start);
	printf("Enter end : \n");
	scanf("%d", &end);

	printf("\n");

	for(int i=start; i<=end; i++){
		if(i%2==0){
			printf("%d\n", i*i*i);
		}else{
			printf("%d\n", i);
		}
	}
}
