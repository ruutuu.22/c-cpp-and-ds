/*
 * d d d d
 * c c c c 
 * b b b b
 * a a a a
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		char ch = 'a' + rows - i;
		for(int j=1; j<=rows; j++){
			printf("%c ", ch);
		}
		printf("\n");
	}
}
