/*
 * a B c
 * d E f
 * g H i
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");

	char ch_small = 'a';
	char ch_capital = 'A';

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			if(j%2!=0){
				printf("%c ", ch_small);
			}else{
				printf("%c ", ch_capital);
			}
			ch_small++;
			ch_capital++;
		}
		printf("\n");
	}
}
