/*
 * ROWS = 3
 * 3
 * 6 9
 * 12 15 18
 *
 * ROWS = 4
 * 4
 * 8 12
 * 16 20 24
 * 28 32 36 40
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	int x = rows;

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=i; j++){
			printf("%d\t", x);
			x+=rows;
		}
		printf("\n");
	}
}

