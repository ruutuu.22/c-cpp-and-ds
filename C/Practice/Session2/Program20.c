/*
 * 1
 * 2 4
 * 3 6 9
 * 4 8 12 16
 */
 

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		int x = i;
		for(int j=1; j<=i; j++){
			printf("%d ", x);
			x+=i;
		}
		printf("\n");
	}
}
