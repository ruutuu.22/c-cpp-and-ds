/*
 *  9  64   7 
 * 36   5  15
 *  3   4   1
 */

#include<stdio.h>
void main(){
	int row;
	printf("Enter row: \n");
	scanf("%d", &row);
	printf("\n");
	int x = row*row;
	for(int i=1; i<=row; i++){
		for(int j=1; j<=row;j++){
			if(x%2==0){
				printf("%d\t", x*x);
			}else{
				printf("%d\t", x);
			}
			x--;
		}
		printf("\n");
	}
}
