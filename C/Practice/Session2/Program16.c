
/*
 *  1  4  7
 * 10 13 16
 * 19 22 25
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");

	int x = 1;

	for(int i=1; i<=rows; i++){
		for(int j=1; j<=rows; j++){
			printf("%d\t", x);
			x+=3;
		}
		printf("\n");
	}
}
