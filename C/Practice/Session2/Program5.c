/*
 * a
 * b c
 * d e f 
 * g h i j 
 */

#include<stdio.h>
void main(){
	int row;
	printf("Enter row:\n");
	scanf("%d", &row);
	char ch = 'a';
	printf("\n");

	for(int i=1; i<=row; i++){
		for(int j=1; j<=i; j++){
			printf("%c\t", ch);
			ch++;
		}
		printf("\n");
	}
}
