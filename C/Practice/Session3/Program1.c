
/*
 * FActorial of a no.
 */

#include<stdio.h>
void main(){
	int num, fact=1;
	printf("Enter num: \n");
	scanf("%d", &num);
	int num1 = num;
	while(num1!=0){
		fact = fact*num1;
		num1--;
	}
	printf("Factorial of %d is %d.\n", num, fact);
}
