/*
 * 1 2 3 4 5
 * 2 3 4 5 
 * 3 4 5
 * 4 5
 * 5
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter row: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows;i++){
		int x = i;
		for(int j = rows; j>=i; j--){
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}
}
