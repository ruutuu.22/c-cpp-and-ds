/*
 * 	* * * * 
 * 	* * *
 * 	* *
 * 	*
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter row: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows;i++){
		for(int j = rows; j>=i; j--){
			printf("* ");
		}
		printf("\n");
	}
}
