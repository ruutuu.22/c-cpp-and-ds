/*
 * 4 3 2 1 
 * C B A
 * 2 1 
 * A
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter row: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows;i++){
		char ch = 'A' + rows - i;
		int x = 1 + rows - i;
		for(int j = rows; j>=i; j--){
			if(i%2==0){
				printf("%c ", ch);
				ch--;
			}else{
				printf("%d ", x);
				x--;
			}
		}
		printf("\n");
	}
}
