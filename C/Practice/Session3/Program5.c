/*
 * 4 3 2 1
 * 3 2 1
 * 2 1
 * 1
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter row: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows;i++){
		int x = rows - i + 1;
		for(int j = rows; j>=i; j--){
			printf("%d ", x);
			x--;
		}
		printf("\n");
	}
}
