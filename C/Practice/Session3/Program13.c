
/*
 * Factorial of a numbers in range.
 */

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start: \n");
	scanf("%d", &start);
	printf("Enter end: \n");
	scanf("%d", &end);
	for(int i = start; i<=end; i++){
		int fact = 1;
		for(int j=1; j<=i; j++){
			fact = fact*j;
		}
		printf("Factorial of %d is %d.\n", i, fact);
	}
}
