/*
 * rows = 4
 * d d d d
 * C C C
 * b b 
 * A
 *
 * rows = 3
 * c c c
 * B B 
 * a
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter row: \n");
	scanf("%d", &rows);

	printf("\n");
	char ch_small ='a' + rows - 1;
	char ch_capital = 'A' + rows - 1;

	for(int i=1; i<=rows;i++){
		for(int j = rows; j>=i; j--){
			if(i%2==0){
				printf("%c ", ch_capital);
			}else{
				printf("%c ", ch_small);
			}
		}
		ch_capital--;
		ch_small--;
		printf("\n");
	}
}
