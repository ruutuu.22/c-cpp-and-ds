/*
 *	      d
 *	    c c c 
 *	  b b b b b 
 * 	a a a a a a a
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	for(int i=1; i<=rows; i++){
		char ch = 'a'+rows-i;
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			printf("%c ", ch);
		}
		printf("\n");
	}
}
