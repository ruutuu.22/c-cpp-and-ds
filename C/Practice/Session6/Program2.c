/*
 *	      4
 *	    3 3 3 
 *	  2 2 2 2 2 
 * 	1 1 1 1 1 1 1
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	int x = rows;
	for(int i=1; i<=rows; i++){
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			printf("%d ", x);
		}
		x--;
		printf("\n");
	}
}
