/*
 *       1
 *     A b A
 *   1 2 3 2 1
 * A b C d C b A
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		char ch_s = 'a';
		char ch_c = 'A';
		int x = 1;
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			if(i%2!=0){
				if(j<i){
					printf("%d ", x);
					x++;
				}else{
					printf("%d ", x);
					x--;
				}
			}else{
				if(j<i){
					if(x%2!=0){
						printf("%c ", ch_c);
					}else{
						printf("%c ", ch_s);
					}
					ch_s++;
					ch_c++;
				}else{
					if(x%2!=0){
						printf("%c ", ch_c);
					}else{
						printf("%c ", ch_s);
					}
					ch_s--;
					ch_c--;
				}
				x++;
			}
		}
		printf("\n");
	}
}

