/*
 *         5
 *       5 6 5 
 *     5 4 3 4 5
 *   5 6 7 8 7 6 5
 * 5 4 3 2 1 2 3 4 5
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		int x = rows;
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			if(i%2==0){
				if(j<i){
					printf("%d ", x);
					x++;
				}else{
					printf("%d ", x);
					x--;
				}
			}else{

				if(j<i){
					printf("%d ", x);
					x--;
				}else{
					printf("%d ", x);
					x++;
				}
			}
		}
		printf("\n");
	}
}

