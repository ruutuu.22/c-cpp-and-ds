/*
 *	      1
 *	    1 2 1
 *	  1 2 3 2 1
 * 	1 2 3 4 3 2 1
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	for(int i=1; i<=rows; i++){
		int x = 1;
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			if(j<i){
				printf("%d ", x);
				x++;
			}else{
				printf("%d ", x);
				x--;
			}
		}
		printf("\n");
	}
}
