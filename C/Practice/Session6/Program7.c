/*
 *				1
 *			4	7	4
 *		7	10	13	10	7
 * 	10	13	16	19	16	13	10
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	int x = 1;
	for(int i=1; i<=rows; i++){
		int x = 3*(i-1) + 1;
		for(int sp=rows; sp>i; sp--){
			printf(" \t");
		}
		for(int j=1; j<=i+i-1; j++){
			if(j<i){
				printf("%d\t", x);
				x+=3;
			}else{
				printf("%d\t", x);
				x-=3;
			}
		}
		printf("\n");
	}
}
