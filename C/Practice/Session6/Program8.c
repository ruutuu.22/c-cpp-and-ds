/*
 *				D
 *			c	D	c
 *		B	c	D	c	B
 * 	a	B	c	D	c	B	a
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	int x = 1;
	for(int i=1; i<=rows; i++){
		char ch_s = 'a'+rows-i;
		char ch_c = 'A'+rows-i;
		for(int sp=rows; sp>i; sp--){
			printf(" \t");
		}
		for(int j=1; j<=i+i-1; j++){
			if(x%2!=0){
				if(j<i){
					printf("%c\t", ch_c);
					ch_c++;
					ch_s++;

				}else{
					printf("%c\t", ch_c);
					ch_c--;
					ch_s--;
				}
			}else{
				if(j<i){
					printf("%c\t", ch_s);
					ch_s++;
					ch_c++;
				}else{
					printf("%c\t", ch_s);
					ch_s--;
					ch_c--;
				}
			}
			x++;
		}
		printf("\n");
	}
}
