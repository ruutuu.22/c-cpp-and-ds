/*
 *         A
 *       b a b
 *     C E G E C
 *   d c b a b c d 
 * E G I K M K I G E
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	for(int i=1; i<=rows; i++){
		char ch_s = 'a' + i - 1;
		char ch_c = 'A' + i - 1;
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			if(i%2==0){
				if(j<i){
					printf("%c ", ch_s);
					ch_s--;
				}else{
					printf("%c ", ch_s);
					ch_s++;
				}
			}else{

				if(j<i){
					printf("%c ", ch_c);
					ch_c+=2;
				}else{
					printf("%c ", ch_c);
					ch_c-=2;
				}
			}
		}
		printf("\n");
	}
}

