/*
 *	      d
 *	    C C C  
 *	  b b b b b 
 * 	A A A A A A A
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);
	for(int i=1; i<=rows; i++){
		char ch_s = 'a'+rows-i;
		char ch_c = 'A'+rows-i;
		for(int sp=rows; sp>i; sp--){
			printf("  ");
		}
		for(int j=1; j<=i+i-1; j++){
			if(i%2==0){
				printf("%c ", ch_c);
			}else{
				printf("%c ", ch_s);
			}
		}
		printf("\n");
	}
}
