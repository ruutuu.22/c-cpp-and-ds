/*
 * a b c d 
 * e f g h 
 * i j k l 
 * m n o p
 */

#include<stdio.h>
void main(){
	int row;
	char ch = 'a';

	printf("Enter row: \n");
	scanf("%d", &row);
	for(int i=1; i<=row; i++){
		for(int j=1; j<=row;j++){
			printf("%c ", ch);
			ch++;
		}
		printf("\n");
	}
}

