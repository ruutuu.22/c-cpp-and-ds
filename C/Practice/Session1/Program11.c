/*
 * 1 2 3 4
 * a b c d
 * 5 6 7 8
 * e f g h
 */

#include<stdio.h>
void main(){
	int row, x=1;
	char ch = 'a';

	printf("Enter rows: \n");
	scanf("%d", &row);

	for(int i=1; i<=row; i++){
		for(int j=1; j<=row; j++){
			if(i%2==0){
				printf("%c ", ch);
				ch++;
			}else{
				printf("%d ", x);
				x++;
			}
		}
		printf("\n");
	}
}
