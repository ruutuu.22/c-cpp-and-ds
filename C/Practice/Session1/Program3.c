/*
 * 1  2  3  4
 * 5  6  7  8 
 * 9 10 11 12
 */

#include<stdio.h>
void main(){
	int row, col, x=1;
	printf("Enter row: \n");
	scanf("%d", &row);
	printf("Enter column: \n");
	scanf("%d", &col);
	for(int i=1; i<=row; i++){
		for(int j=1; j<=col;j++){
			printf("%d\t", x);
			x++;
		}
		printf("\n");
	}
}

