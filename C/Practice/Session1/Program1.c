

/*
 * write a program to to print the square of odd numbers between given range from user.
 */

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start: \n");
	scanf("%d", &start);
	printf("Enter end: \n");
	scanf("%d", &end);

	for(int i=start; i<=end;i++){
		if(i%2!=0){
			printf("%d\n", i*i);
		}
	}
}
