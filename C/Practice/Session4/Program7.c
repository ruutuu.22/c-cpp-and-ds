/*
 * 	   1
 *       A b
 *     1 2 3
 *   A b C d
 * 1 2 3 4 5
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		char ch_cap = 'A';
		char ch_sma = 'a';
		int x = 1;
		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i;j++){
			if(i%2==0){
				if(j%2==0){
					printf("%c ", ch_sma);
					ch_sma++;
					ch_cap++;
				}else{
					printf("%c ", ch_cap);
					ch_sma++;
					ch_cap++;
				}
			}else{
				printf("%d ", x);
				x++;
			}
		}
		printf("\n");
	}
}
