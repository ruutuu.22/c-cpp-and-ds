/*
 *        1
 *     4  7 
 * 10 13 15
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");
	int x = 1;
	for(int i=1; i<=rows; i++){
		for(int sp = rows; sp>i; sp--){
			printf(" \t");
		}
		for(int j = 1; j<=i;j++){
			printf("%d\t", x);
			x+=3;
		}
		printf("\n");
	}
}
