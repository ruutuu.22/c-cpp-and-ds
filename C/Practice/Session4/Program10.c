/*
 *       1
 *     1 2 3 
 *   1 2 3 4 5
 * 1 2 3 4 5 6 7 
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		int x = 1;
		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i+i-1;j++){
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}
}
