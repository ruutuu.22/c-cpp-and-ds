/*
 *       4
 *     3 6
 *   2 4 6
 * 1 2 3 4
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		int x = rows - i + 1;
		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i; j++){
			printf("%d ", x);
			x = x + rows - i + 1;
		}
		printf("\n");
	}
}

