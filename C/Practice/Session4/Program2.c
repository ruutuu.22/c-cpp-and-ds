/*
 *       4
 *     4 3 
 *   4 3 2
 * 4 3 2 1
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		int x = rows;
		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i;j++){
			printf("%d ", x);
			x--;
		}
		printf("\n");
	}
}
