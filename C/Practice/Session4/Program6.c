/*
 * 	   A
 *       b a
 *     C E G
 *   d c b a
 * E G I K M
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		char ch_cap = 'A' + i - 1;
		char ch_sma = 'a' + i - 1;
		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i;j++){
			if(i%2==0){
				printf("%c ", ch_sma);
				ch_sma--;
			}else{
				printf("%c ", ch_cap);
				ch_cap+=2;
			}
		}
		printf("\n");
	}
}
