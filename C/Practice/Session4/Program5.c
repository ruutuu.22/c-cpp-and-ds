/*
 * 	   5
 *       5 6
 *     5 4 3
 *   5 6 7 8
 * 5 4 3 2 1
 */

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows:\n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		int x = rows;
		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i;j++){
			if(i%2==0){
				printf("%d ", x);
				x++;
			}else{
				printf("%d ", x);
				x--;
			}
		}
		printf("\n");
	}
}
