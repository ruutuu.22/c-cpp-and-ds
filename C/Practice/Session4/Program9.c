/*
 *        D
 *      c D
 *    B c D
 *  a B c D
 */


#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows: \n");
	scanf("%d", &rows);

	printf("\n");

	for(int i=1; i<=rows; i++){
		char ch_cap = 'A' + rows - i;
		char ch_sma = 'a' + rows - i;

		for(int sp = rows; sp>i; sp--){
			printf("  ");
		}
		for(int j = 1; j<=i;j++){
			if(i%2==0){
				if(j%2==0){
					printf("%c ", ch_cap);
					ch_cap++;
					ch_sma++;
				}else{
					printf("%c ", ch_sma);
					ch_cap++;
					ch_sma++;
				}
			}else{
				if(j%2!=0){
					printf("%c ", ch_cap);
					ch_cap++;
					ch_sma++;
				}else{
					printf("%c ", ch_sma);
					ch_cap++;
					ch_sma++;
				}
			}
		}
		printf("\n");
	}
}

