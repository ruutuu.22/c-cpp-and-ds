/*
 * WAP to print the addition factorials of two given numbers from user.
 */

#include<stdio.h>
int fact = 1;
int facto(int);
void main(){
	int num1, num2;
	printf("Enter num1:\n");
	scanf("%d", &num1);
	printf("Enter num2:\n");
	scanf("%d", &num2);
	int fact1 = facto(num1);
	int fact2 = facto(num2);

	printf("Addition of factorials of %d and %d is %d.\n", num1, num2, fact1+fact2);
}
int facto(int n){
	fact = 1;
	for(int i=1; i<=n; i++){
		fact = fact * i;
	}
	return fact;
}

