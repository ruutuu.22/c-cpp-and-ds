/*
 * conatinating two LINKED LIST
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct LL{
	int data;
	struct LL *next;
}LL;

LL *head1 = NULL;
LL *head2 = NULL;

LL *createNode(){
	LL *nn = (LL*)malloc(sizeof(LL));
	printf("Enter data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;
	return nn;
}

void addNode(LL **head){
	LL *nn = createNode();
	if(*head == NULL){
		*head = nn;
	}else{
		LL *temp = *head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(LL *head){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		LL *temp = head;
		while(temp->next != NULL){
			printf("[ %d ] -> ", temp->data);
			temp = temp->next;
		}
		printf("[ %d ]\n", temp->data);
	}
}

void concatLL(){
	if(head1 == NULL && head2 == NULL){
		printf("Both LL are empty.\n");
	}else if(head2 == NULL){
		printf("Second LL is EMPTY.\n");
	}else if(head1 == NULL){
		head1 = head2;
	}else{
		LL *temp = head1;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = head2;
	}
}

void main(){
	int nodes;

	printf("Enter the number of nodes in LINKED LIST1 : \n");
	scanf("%d", &nodes);
	for(int i=1; i<=nodes; i++){
		addNode(&head1);
	}

	printf("Enter the number of nodes in LINKED LIST2 : \n");
	scanf("%d", &nodes);
	for(int i=1; i<=nodes; i++){
		addNode(&head2);
	}

	printf("Before Concatination :\n");
	printLL(head1);
	printLL(head2);
	
	printf("After concatination :\n");
	concatLL();
	printLL(head1);
	printLL(head2);
}
