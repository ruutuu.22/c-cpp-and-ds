/*
 * Priority Queue using LL
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	int priority;
	struct demo *next;
}dm;

dm *head = NULL;

dm *createNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter data: \n");
	scanf("%d", &nn->data);

	printf("Enter priority : \n");
	while(1){
		scanf("%d", &nn->priority);
		if(nn->priority >= 0 && nn->priority <= 5){
			break;
		}
		printf("Re-enter the priority.\n");
	}

	nn->next = NULL;

	return nn;
}

void addNode(dm *nn){
	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void addFirst(dm *nn){
	nn->next = head;
	head = nn;
}

void addAtPos(dm *nn, int pos){
	dm *temp = head;
	while(pos - 2){
		temp = temp->next;
		pos --;
	}
	nn->next = temp->next;
	temp->next = nn;
}

void enqueue(){
	dm *nn = createNode();
	if(head == NULL){
		addNode(nn);
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		if(head->priority > nn->priority){
			addFirst(nn);
		}else if(temp->priority <= nn->priority){
			addNode(nn);
		}else{
			dm *temp1 = head;
			int count = 0;

			while(temp1 != NULL){
				count++;
				if(temp1->priority > nn->priority){	
					break;
				}
				temp1 = temp1->next;
			}
			addAtPos(nn, count);
		}
	}
}

int printQueue(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			printf("[%d | %d]->", temp->data, temp->priority);
			temp = temp->next;
		}
		printf("[%d | %d]\n", temp->data, temp->priority);
	}
}

void main(){
	char choice;
	do{
		 printf("Make your choice :\n");
		 printf("1.Enqueue.\n2.Print Queue.\n");
			
		 int ch;
		 printf("Enter your choice:\n");
		 scanf("%d", &ch);

		 switch(ch){
			 case 1:
				 enqueue();
				 break;

			 case 2:
				 printQueue();
				 break;

			 default:
				 printf("Invalid Choice.\n");
		 }

		 getchar();
		 printf("Do you want to continue:\n");
		 scanf("%c", &choice);
	 }while(choice == 'Y' || choice == 'y');
}

