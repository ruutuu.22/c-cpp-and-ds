/*
 * Priority Queue
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	int priority;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node  *nn = (node*)malloc(sizeof(node));
	printf("Enter Data: \n");
	scanf("%d", &nn->data);

	printf("Enter Priority :\n");
	while(1){
		scanf("%d", &nn->priority);
		if(nn->priority >= 0 && nn->priority <= 5){
			break;
		}
		printf("Re-enter the priority: \n");
	}

	nn->next = NULL;

	return nn;
}
void enqueue(){
	node *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		int flag = 0;
		node *temp = head;
		node *prev = NULL;
		node *prev1 = NULL;
		int count = 0;

		while(temp != NULL){
			prev1 = prev;
			prev = temp;
			count++;
			if(nn->priority < temp->priority){
				flag = 1;
				break;
			}
			temp = temp->next;
		}

		if(count == 1 && flag == 1){
			nn->next = head;
			head = nn;
		}else if (flag == 1){
			nn->next = prev1->next;
			prev1->next = nn;
		}else{
			prev->next = nn;
		}
	}
}

int printQueue(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		node *temp = head;
		while(temp->next != NULL){
			printf("[%d | %d]->", temp->data, temp->priority);
			temp = temp->next;
		}
		printf("[%d | %d]\n", temp->data, temp->priority);
	}
}

void main(){
	char choice;
	do{
		 printf("Make your choice :\n");
		 printf("1.Enqueue.\n2.Print Queue.\n");
			
		 int ch;
		 printf("Enter your choice:\n");
		 scanf("%d", &ch);

		 switch(ch){
			 case 1:
				 enqueue();
				 break;

			 case 2:
				 printQueue();
				 break;

			 default:
				 printf("Invalid Choice.\n");
		 }

		 getchar();
		 printf("Do you want to continue:\n");
		 scanf("%c", &choice);
	 }while(choice == 'Y' || choice == 'y');
}

