/*
 * Implementing Circular Queue using Linked list
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct QUEUE{
	int data;
	struct QUEUE *next;
}queue;

queue *front = NULL;
queue *rear = NULL;

int flag = 0;
int size = 0;
int count = 0;

queue *createNode(){
	queue *nn = (queue*)malloc(sizeof(queue));
	if(nn == NULL){
		printf("Memory Full.\n");
		exit(0);//included in header file (#inclde<stdlib.h>)
	}

	printf("Enter data in QUEUE:\n");
	scanf("%d", &nn->data);

	nn->next = NULL;

	return nn;
}

int enqueue(){
	if(count == size){
		return -1;
	}else{
		count++;
		queue *nn = createNode();
		if(front == NULL){
			front = nn;
			rear = nn;
			front->next = front;
		}else{	
			rear->next = nn;
			rear = rear->next;
			rear->next = front;
		}
		return 0;
	}
}

int dequeue(){
	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		count--;
		flag = 1;
		int ret = front->data;
		if(front == rear){
			free(front);
			front = NULL;
			rear = NULL;
		}else{
			front = front->next;
			free(rear->next);
			rear->next = front;
		}
		return ret;
	}
}

int Frontt(){
	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return front->data;
	}
}

int printQ(){
	if(front == NULL){
		return -1;
	}else{
		queue *temp = front;
		while(temp != rear){
			printf("%d ", temp->data);
			temp=temp->next;
		}
		printf("%d\n", temp->data);
	}
}

void main(){
	printf("Enter the size of QUEUE:\n");
	scanf("%d", &size);
	char choice;

	do{
		printf("Make your choice:\n");
		printf("1.Enqueue.\n2.Dequeue.\n3.Front.\n4.Print Queue.\n");

		int ch;
		printf("Enter your choice;\n");
		scanf("%d", &ch);

		switch(ch){
			case 1:{
				       int ret = enqueue();
				       if(ret == -1){
					       printf("Queue Overflow.\n");
				       }
			       }
				break;

			case 2:
				{
					int ret = dequeue();
					if(flag == 0){
						printf("Queue Underflow.\n");
					}else{
						printf("Dequeued data is %d.\n", ret);
					}
				}
				break;

			case 3:
				{
					int ret = Frontt();
					if(flag == 0){
						printf("Queue Underflow.\n");
					}else{
						printf("Data at FRONT is %d.\n", ret);
					}
				}
				break;

			case 4:
				{
					int ret = printQ();
					if(ret == -1){
						printf("Queue Underflow.\n");
					}
				}
				break;

			default:
				printf("Invalid Choice.\n");
		}
		getchar();
		printf("Do you want to continue?...Y/N.\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
