/*
 * Implementing Circular Queue using Array
 */

#include<stdio.h>

int size = 0;
int front = -1;
int rear = -1;
int flag = 0;

int enqueue(int *queue){
	if((rear == size - 1 && front == 0) || rear == front - 1){
		return -1;
	}else{
		if(front == -1){
			front++;
		}else{
			if(rear == size - 1 && front != 0){
				rear = -1;
			}
		}
		rear++;
		printf("Enter data in QUEUE:\n");
		scanf("%d",&queue[rear]);
		return 0;
	}
}

int dequeue(int *queue){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int ret = queue[front];
		if(front == rear){
			front = -1;
			rear = -1;
		}else{
			if(front == size - 1){
				front = -1;
			}
			front++;
		}
		return ret;
	}
}

int Frontt(int *queue){
	if(front == -1){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return queue[front];
	}
}

int printCQ(int *queue){
	if(front == -1){
		return -1;
	}else{
		int i = front;
		while(i != rear){
			printf("%d ", queue[i]);
			if(i == size - 1){
				i = -1;
			}
			i++;
		}
		printf("%d\n", queue[i]);
		return 0;
	}
}

void main(){
	printf("Enter the size of queue:\n");
	scanf("%d", &size);

	int queue[size];
	char choice;

	do{
		printf("Make your choice:\n");
		printf("1.Enqueue.\n2.Dequeue.\n3.Front.\n4.Print Circular Queue.\n");

		int ch;
		printf("Enter your choice:\n");
		scanf("%d", &ch);

		switch(ch){
			case 1:
				{
					int ret = enqueue(queue);
					if(ret == -1){
						printf("Queue Overflow.\n");
					}
				}
				break;

			case 2:
				{
					int ret = dequeue(queue);
					if(flag == 0){
						printf("Queue Underflow.\n");
					}else{
						printf("Dequeued data is %d.\n", ret);
					}
				}
				break;

			case 3:
				{
					int ret = Frontt(queue);
					if(flag == 0){
						printf("Queue Underflow.\n");
					}else{
						printf("Data at FRONT is %d.\n", ret);
					}
				}
				break;

			case 4:
				{
					int ret = printCQ(queue);
					if(ret == -1){
						printf("Queue Empty.\n");
					}
				}
				break;

			default:
				printf("Invalid Choice.\n");
		}
		getchar();
		printf("Do you want to continue?...Y/N.\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
