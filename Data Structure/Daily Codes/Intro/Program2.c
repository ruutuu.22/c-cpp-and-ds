/*
 * sturcture pointer
 */

#include<stdio.h>
void main(){
	char carr[5] = {'A','B', 'C', 'D', 'E'};

	char *cptr = carr;
	int *iptr = carr;

	printf("cptr = %c\n", *cptr);
	printf("iptr = %c\n", *iptr);

	cptr++;
	iptr++;

	printf("cptr = %c\n", *cptr);
	printf("iptr = %c\n", *iptr);
}
