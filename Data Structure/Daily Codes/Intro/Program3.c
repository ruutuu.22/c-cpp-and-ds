/*
 * sturcture pointer
 */

#include<stdio.h>

struct Movie{
	char mName[10];
	int count;
	float rating;
}obj1 = {"Drishyam", 5, 8.8};

void main(){
	typedef struct Movie mv;
	mv obj2 = {"kantara", 10, 9.5};
	mv *ptr1 = &obj1;
	mv *ptr2 = &obj2;	

	printf("mName = %s\n", (*ptr1).mName);
	printf("count = %d\n", (*ptr1).count);
	printf("rating = %f\n", (*ptr1).rating);

	printf("mName = %s\n", (*ptr2).mName);
	printf("count = %d\n", (*ptr2).count);
	printf("rating = %f\n", (*ptr2).rating);
}
