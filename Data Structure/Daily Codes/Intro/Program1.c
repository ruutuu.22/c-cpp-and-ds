/*
 * structure
 */

#include<stdio.h>
#include<string.h>

struct CricPlayer{
	int jerNo;
	char pName[10];
	float avg;
}obj1 = {18, "Virat", 49.56};

void main(){
	struct CricPlayer obj2;
	obj2.jerNo = 45;
	strcpy(obj2.pName, "Rohit");
	obj2.avg = 48.58;

	printf("jerNo = %d\n", obj1.jerNo);
	printf("pName = %s\n", obj1.pName);
	printf("avg = %f\n", obj1.avg);

	printf("jerNo = %d\n", obj2.jerNo);
	printf("pName = %s\n", obj2.pName);
	printf("avg = %f\n", obj2.avg);
}

