/*
 * malloc() := void *malloc(sizeof(size_t));
 */

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct ott{
	char pName[10];
	int userCount;
	float price;
}OTT;

void main(){
	OTT *ptr = (OTT*)malloc(sizeof(OTT));
	strcpy(ptr->pName, "Prime");
	ptr->userCount = 10568;
	ptr->price = 157.6;

	OTT *ptr1 = (OTT*)malloc(sizeof(OTT));
	strcpy(ptr1->pName, "Hotstar");
	ptr1->userCount = 12568;
	ptr1->price = 179.6;
	
	printf("%s\n", ptr->pName);
	printf("%d\n", ptr->userCount);
	printf("%f\n", ptr->price);

	printf("%s\n", ptr1->pName);
	printf("%d\n", ptr1->userCount);
	printf("%f\n", ptr1->price);
}
