


#include<stdio.h>

void fun(int *a){
	*a = *a + 10;
	printf("*a = %d\n", *a);
}

void main(){
	int x = 10;
	fun(&x);
	printf("x = %d\n", x);
}
