/*
 * malloc()
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct cricLeague{
	char lName[10];
	int totalMatches;
	float priceAmt;
}cl;

void main(){
	cl * ptr = (cl*)malloc(sizeof(cl));
	strcpy(ptr->lName, "IPL");
	ptr->totalMatches = 74;
	ptr->priceAmt = 20.50;

	printf("League name = %s\n", ptr->lName);
	printf("No. of Total Matches = %d\n", ptr->totalMatches);
	printf("Price Amount = %f\n", ptr->priceAmt);
}
