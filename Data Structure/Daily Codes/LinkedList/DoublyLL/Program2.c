/*
 * Reverse Doubly Linked List
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node * createNode(){
	Node * nn = (Node*)malloc(sizeof(Node));

	nn->prev = NULL;
	printf("Enter Integer data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;

	return nn;
}

void addNode(){
	Node *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp=temp->next;
		}
		temp->next = nn;
		nn->prev = temp;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("{ %d } --> ", temp->data);
			temp = temp->next;
		}
		printf("{ %d }\n", temp->data);
	}
}

//Below function doesn't change the LL but only prints the LL in reverse way
void revDoublyLL_1(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if (head->next == NULL){
		printf("Only ONE node present.\n{ %d }\n", head->data);
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		while(temp->prev != NULL){
			printf("{ %d } --> ", temp->data);
			temp = temp->prev;
		}
		printf("{ %d }\n", temp->data);
	}
}

int countNode(){
	Node *temp = head;
	int count = 0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

//below function make changes to totally reverse the LL
void revDoublyLL_2(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if (head->next == NULL){
		printf("Only ONE node present.\n{ %d }\n", head->data);
	}else{
		int count = countNode();
		Node *temp1 = head;
		Node *temp2 = head;
		while(temp2->next != NULL){
			temp2 = temp2->next;
		}

		int cnt = count/2;
		while(cnt){
			int temp = temp1->data;
			temp1->data = temp2->data;
			temp2->data = temp;

			temp1 = temp1->next;
			temp2 = temp2->prev;
			cnt--;
		}
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();
	printf("\nPrinting Doubly LL in Reverse(LL not stored in reverse form).\n");
	revDoublyLL_1();
	if(head != NULL && head->next != NULL){
		printf("If LL is not stored in Reverse form. Calling printLL function will give:\n");
		printLL();
	}

	printf("\nPrinting doubly LL in Reverse(LL stored in reverse form).\n");
	revDoublyLL_2();
	if(head != NULL && head->next != NULL){
		printLL();
	}
}
