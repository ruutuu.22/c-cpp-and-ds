/*
 * real time example : CALL OF DUTY
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct COD{
	struct COD *prev;
	char pName[10];
	int kills;
	float kd;
	struct COD *next;  
}COD;

COD *head = NULL;

COD * createNode(){
	COD *nn = (COD*)malloc(sizeof(COD));
	
	nn->prev = NULL;

	getchar();
	printf("Enter Player Name:\n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->pName[i] = ch;
		i++;
	}

	printf("Enter the number of kils:\n");
	scanf("%d", &nn->kills);

	printf("Enter the Kill Death Ratio :\n");
	scanf("%f", &nn->kd);
	nn->next = NULL;

	return nn;
}

void addNode(){
	COD *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		COD *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
		nn->prev = temp;
	}
}

void addFirst(){
	COD *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		nn->next = head;
		head->prev = nn;
		head = nn;
	}
}

void addLast(){
	addNode();
}

int countNode(){
	COD *temp = head;
	int count = 0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

int addAtPos(int pos){
	int count = countNode();
	if(pos <= 0 || pos >= count+2){
		printf("Entered position is invalid.\n");
		return -1;
	}else{
		if(pos == 1){
			addFirst();
		}else if(pos == count+1){
			addLast();
		}else{
			COD *nn = createNode();
			COD *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			nn->next = temp->next;
			nn->prev = temp;
			temp->next->prev = nn;
			temp->next = nn;
		}
		return 0;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		COD *temp = head;
		while(temp->next != NULL){
			printf("[ %s |", temp->pName);
			printf(" %d |", temp->kills);
			printf(" %f ] => ", temp->kd);
			temp = temp->next;
		}
		printf("[ %s |", temp->pName);
		printf(" %d |", temp->kills);
		printf(" %f ]\n", temp->kd);		
	}
}

void deleteFirst(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if( head->next == NULL){
		free(head);
		head = NULL;
	}else{
		head = head->next;
		free(head->prev);
		head->prev = NULL;
	}
}

void deleteLast(){
	if(head == NULL || head->next == NULL){
		deleteFirst();
	}else{
		COD *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}

int deleteAtPos(int pos){
	int count = countNode();
	if(pos<=0 || pos > count){
		printf("Entered position is invalid.\n");
		return -1;
	}else{
		if(pos == 1){
			deleteFirst();
		}else if(pos == count){
			deleteLast();
		}else{
			COD *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			temp->next = temp->next->next;
			free(temp->next->prev);
			temp->next->prev = temp;
		}
		return 0;
	}
}

void main(){
	char choice;
	do{
		printf("------MAKE A CHOICE------\n");
		printf("\t1.Add a Node.\n");
		printf("\t2.Add Node at First.\n");
		printf("\t3.Add Node at Last.\n");
		printf("\t4.Add Node at Particular Position.\n");
		printf("\t5.Count Nodes.\n");
		printf("\t6.Delete Node at First.\n");
		printf("\t7.Delete Node at Last.\n");
		printf("\t8.Delete Node at Particular POsition.\n");
		printf("\t9.Print Linked List.\n");

		printf("\n");

		int ch;
		printf("Enter your choice:\n");
		scanf("%d", &ch);

		printf("\n");

		switch(ch){
			case 1:
				addNode();
				break;

			case 2:
				addFirst();
				break;

			case 3:
				addLast();
				break;

			case 4:
				{
					int pos;
					printf("Enter a position to add a node:\n");
					scanf("%d", &pos);
					addAtPos(pos);
				}
				break;

			case 5:
				{
					int count = countNode();
					printf("Node count is %d.\n", count);
				}
				break;

			case 6:
				deleteFirst();
				break;

			case 7:
				deleteLast();
				break;

			case 8:
				{
					int pos;
					printf("Enter a position to delete a node:\n");
					scanf("%d", &pos);
					deleteAtPos(pos);
				}
				break;

			case 9:
				printLL();
				break;

			default:
				printf("INVALID CHOICE.......\n");

		}
	
		getchar(); 
		printf("Do you want to continue?.....Y/n.\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
