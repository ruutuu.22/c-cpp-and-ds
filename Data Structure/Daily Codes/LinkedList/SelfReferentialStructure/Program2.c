/*
 * self referential structure :- structure that refers the structure that is same as it is.
 */

#include<stdio.h>
#include<string.h>

typedef struct Batsmen{
	int jerNo;
	char bName[10];
	float avg;
	struct Batsmen *next;
}bat;

void main(){
	bat obj1, obj2, obj3;
	
	bat *head = &obj1;


	head->jerNo = 45;		
	//obj1.jerNo = 45;	
	strcpy(head->bName, "Rohit");
	//strcpy(obj1.bName, "Rohit");
	head->avg = 50.54;
	//obj1.avg = 50.54;
	head->next = &obj2;
	//obj1.next = &obj2;


	head->next->jerNo = 18;
	//obj2.jerNo = 2;
	strcpy(head->next->bName, "Virat");
	//strcpy(obj2.bName,"Virat");
	head->next->avg = 49.54;
	//obj2.avg = 49.54;
	head->next->next = &obj3;
	//obj2.next = &obj3;


	head->next->next->jerNo = 7;
	//obj3.jerNo = 3;
	strcpy(head->next->next->bName, "Dhoni");
	//strcpy(obj3.bName,"Dhoni");
	head->next->next->avg = 48.54;
	//obj3.avg = 48.54;
	head->next->next->next = NULL;
	//onj3.next = NULL;
	

	printf("%d\n", head->jerNo);
	printf("%s\n", head->bName);
	printf("%f\n", head->avg);

	printf("%d\n", head->next->jerNo);
	printf("%s\n", head->next->bName);
	printf("%f\n", head->next->avg);
	
	printf("%d\n", head->next->next->jerNo);
	printf("%s\n", head->next->next->bName);
	printf("%f\n", head->next->next->avg);
}
