
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{
	char mName[20];
	int count;
	float rating;
	struct Movie *next;
}mv;

void putdata(mv *ptr){
	printf("mName: ");
	fgets(ptr->mName, 20, stdin);
	//scanf("%s", &(ptr->mName));
	printf("count: ");
	scanf("%d", &(ptr->count));
	printf("rating: ");
	scanf("%f", &(ptr->rating));
	printf("\n");
	getchar();
}

void accessData(mv *ptr){
	printf("%s", ptr->mName);
	printf("%d\n", ptr->count);
	printf("%f\n", ptr->rating);
	printf("%p\n", ptr->next);
	printf("\n");
}

void main(){
	mv *mptr1 = (mv*)malloc(sizeof(mv));
	mv *mptr2 = (mv*)malloc(sizeof(mv));
	mv *mptr3 = (mv*)malloc(sizeof(mv));

	putdata(mptr1);
	mptr1->next = mptr2;

	putdata(mptr2);
	mptr2->next = mptr3;

	putdata(mptr3);
	mptr3->next = NULL;

	accessData(mptr1);
	accessData(mptr2);
	accessData(mptr3);
}



