/*
 * self referential structure :- structure that refers the structure that is same as it is.
 */

#include<stdio.h>
#include<string.h>

typedef struct employee{
	int empId;
	char empName[10];
	float sal;
	struct employee *next;
}emp;

void main(){
	emp obj1, obj2, obj3;
	
	emp *head = &obj1;


	head->empId = 1;		
	//obj1.empId = 1;	
	strcpy(head->empName, "ABC");
	//strcpy(obj1.empName, "ABC");
	head->sal = 60.54;
	//obj1.sal = 60.54;
	head->next = &obj2;
	//obj1.next = &obj2;


	head->next->empId = 2;
	//obj2.empId = 2;
	strcpy(head->next->empName, "DEF");
	//strcpy(obj2.empName,"DEF");
	head->next->sal = 70.54;
	//obj2.sal = 70.54;
	head->next->next = &obj3;
	//obj2.next = &obj3;


	head->next->next->empId = 3;
	//obj3.empId = 3;
	strcpy(head->next->next->empName, "GHI");
	//strcpy(obj3.empName,"GHI");
	head->next->next->sal = 80.54;
	//obj3.sal = 80.54;
	head->next->next->next = NULL;
	//onj3.next = NULL;
	

	printf("%d\n", head->empId);
	printf("%s\n", head->empName);
	printf("%f\n", head->sal);

	printf("%d\n", head->next->empId);
	printf("%s\n", head->next->empName);
	printf("%f\n", head->next->sal);
	
	printf("%d\n", head->next->next->empId);
	printf("%s\n", head->next->next->empName);
	printf("%f\n", head->next->next->sal);
}
