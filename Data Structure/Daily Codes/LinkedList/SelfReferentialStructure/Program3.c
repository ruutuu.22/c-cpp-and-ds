/*
 * accessing data using function
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{
	int empId;
	char empName[20];
	float sal;
	struct Employee *next;
}emp;

void accessData(emp * ptr){
	printf("%d\n", ptr->empId);
	printf("%s\n", ptr->empName);
	printf("%f\n", ptr->sal);
	printf("%p\n", ptr->next);

	printf("\n");
}

void main(){
	emp * eptr1 = (emp*)malloc(sizeof(emp));	
	emp * eptr2 = (emp*)malloc(sizeof(emp));
	emp * eptr3 = (emp*)malloc(sizeof(emp));

	emp *head = eptr1;

	eptr1->empId = 1;
	//head->empId = 1;
	strcpy(eptr1->empName, "Kanha");
	//strcpy(head->empName, "Kanha");
	eptr1->sal = 60;
	//head->sal = 60;
	eptr1->next = eptr2;
	//head->next = eptr2;

	eptr2->empId = 2;
	//head->next->empId = 2;
	strcpy(eptr2->empName, "Rahul");
	//strcpy(head->next->empName, "Rahul");
	eptr2->sal = 70;
	//head->next->sal = 70;
	eptr2->next = eptr3;
	//head->next->next = eptr3;

	eptr3->empId = 3;
	//haed->next->next->empId = 3;
	strcpy(eptr3->empName, "Ashish");
	//strcpy(head->next->next->empName, "Ashish"); 
	eptr3->sal = 80;
	//head->next->next->sal = 80;
	eptr3->next = NULL;
	//head->next->next->next = NULL;

	accessData(eptr1);
	accessData(eptr2);
	accessData(eptr3);
}
