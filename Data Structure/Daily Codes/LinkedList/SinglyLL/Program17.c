


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}dm;

dm *head = NULL;

dm *createNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;
	return nn;
}
void addNode(){
	dm *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		dm *temp = head;
		while(temp != NULL){
			printf("{ %d } --> ", temp->data);
			temp = temp->next;
		}
		printf("\n");
	}
}
int countNode(){
	int count = 0;
	dm *temp = head;
	while(temp != NULL){
		count++;
		temp=temp->next;
	}
	return count;
}

void occCheck(){
	int occ = 0;
	dm *temp = head;
	dm *temp1 = head;

	int num;
	printf("Enter the number you want to check whether it exists in LL:\n");
	scanf("%d", &num);

	while(temp != NULL){
		if(temp->data == num){
			occ++;
		}
		temp = temp->next;
	}
	int i=0;
	int count = 0;
	while(temp1 != NULL){
		count++;
		if(temp1->data == num){
			i++;
			if(i == occ - 1){
				break;
			}
		}
		temp1=temp1->next;
	}
	if(occ == 1){
		printf("Number exists in LL and has only %d occurence.\n",occ);
	}else if(occ >=2){
		printf("Number exists in LL and has %d occurences.\n", occ);
		printf("Data with last 2nd occurence is at %d position.\n", count);
	}else{
		printf("No occurrences of the entered number.\n");
	}
}

void main(){
	char choice;

	do{
		int ch;
		printf("Make choice:\n");
		printf("1.Add Node.\n");
		printf("2.Print Linked List.\n");
		printf("3.Node Count\n");
		printf("4.Occurence Check.\n");

		printf("Enter your choice:\n");
		scanf("%d", &ch);

		switch(ch){
			case 1:
				addNode();
				break;

			case 2:
				printLL();
				break;
		
			case 3:
				{
					int count = countNode();
					printf("Count of nodes is %d.\n", count);
				}
				break;

			case 4:
				occCheck();
				break;
			default:
				printf("Invalid choice.\n");
		}

		getchar();
		printf("Do you want to continue?....Y/N.\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
