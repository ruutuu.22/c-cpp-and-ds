/*
 * real time example :- BOXING
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Boxer{
	char bName[10];
	int totalMatches;
	float avgFightTime;
	struct Boxer *next;
}box;

box *head = NULL;

box *createNode(){
	box *nn = (box *)malloc(sizeof(box));

	getchar();
	printf("Enter Boxer Name:\n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->bName[i] = ch;
		i++;
	}

	printf("Enter total Number of matches played:\n");
	scanf("%d", &nn->totalMatches);

	printf("Enter Average Fight Time:\n");
	scanf("%f", &nn->avgFightTime);

	nn->next = NULL;

	return nn;
}

void addNode(){
	box *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		box *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void addFirst(){
	box *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		nn->next = head;
		head = nn;
	}
}

void addLast(){
	addNode();
}

int countNode(){
	int count = 0;
	box *temp = head;
	while(temp != NULL){
		count++;
		temp=temp->next;
	}
	return count;
}

int addAtPos(int pos){
	int count = countNode();
	if(pos <= 0 || pos >= count+2){
		printf("Entered position is invalid.\n");
		return -1;
	}else{
		if(pos == 1){
			addFirst();
		}else if(pos == count+1){
			addLast();
		}else{
			box *nn = createNode();
			box *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			nn->next = temp->next;
			temp->next = nn;
		}
		return 0;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		box *temp = head;
		while(temp->next != NULL){
			printf("|| %s ", temp->bName);
			printf("| %d ", temp->totalMatches);
			printf("| %f || --> ", temp->avgFightTime);
			temp=temp->next;
		}
		printf("|| %s ", temp->bName);
		printf("| %d ", temp->totalMatches);
		printf("| %f ||\n", temp->avgFightTime);
	}
}

void deleteFirst(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if(head->next == NULL){
		free(head);
		head = NULL;
	}else{
		box *temp = head;
		head = temp->next;
		free(temp);
	}
}

void deleteLast(){
	if(head == NULL || head->next == NULL){
		deleteFirst();
	}else{
		box *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}

int deleteAtPos(int pos){
	int count = countNode();
	if(pos <= 0 || pos > count){
		printf("Entered position is invalid.\n");
		return -1;
	}else{
		if(pos == 1){
			deleteFirst();
		}else if(pos == count){
			deleteLast();
		}else{
			box *temp = head;
			while(pos - 2){
				temp=temp->next;
				pos--;
			}
			box *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
		return 0;
	}
}

void main(){
	char choice;

	do{
		printf("------MAKE A CHOICE------\n");
		printf("\t1.Add a Node.\n");
		printf("\t2.Add Node at First.\n");
		printf("\t3.Add Node at Last.\n");
		printf("\t4.Add Node at Particular Position.\n");
		printf("\t5.Count Nodes.\n");
		printf("\t6.Delete Node at First.\n");
		printf("\t7.Delete Node at Last.\n");
		printf("\t8.Delete Node at Particular POsition.\n");
		printf("\t9.Print Linked List.\n");

		printf("\n");

		int ch;
		printf("Enter your choice:\n");
		scanf("%d", &ch);

		printf("\n");

		switch(ch){
			case 1:
				addNode();
				break;

			case 2:
				addFirst();
				break;

			case 3:
				addLast();
				break;

			case 4:
				{
					int pos;
					printf("Enter a position to add a node:\n");
					scanf("%d", &pos);
					addAtPos(pos);
				}
				break;

			case 5:
				{
					int count = countNode();
					printf("Node count is %d.\n", count);
				}
				break;

			case 6:
				deleteFirst();
				break;

			case 7:
				deleteLast();
				break;

			case 8:
				{
					int pos;
					printf("Enter a position to delete a node:\n");
					scanf("%d", &pos);
					deleteAtPos(pos);
				}
				break;

			case 9:
				printLL();
				break;

			default:
				printf("INVALID CHOICE.......\n");

		}

		getchar();
		printf("Do you want to continue?......y/n.\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
