/*
 * deleteAtPos(nt pos)
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{
	char name[20];
	int id;
	struct Employee *next;
}emp;

emp *head = NULL;

void addNode(){
	emp *nn = (emp*)malloc(sizeof(emp));

	printf("Enter Name : \n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->name[i] = ch;
		i++;
	}

	printf("Enter ID : \n");
	scanf("%d", &nn->id);

	getchar();

	nn->next = NULL;

	if(head == NULL){
		head = nn;
	}else{
		emp *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		emp *temp = head;
		while(temp != NULL){
			printf("|| %s ", temp->name);
			printf("| %d || --> ", temp->id);
			temp = temp->next;
		}
		printf("\n");
}

int countNode(){
	int count = 0;
	emp *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

void deleteFirst(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if(head->next == NULL){
		free(head);
		head = NULL;
		printf("After deleting Last node.\nLL empty.\n");
	}else{
		emp *temp = head;
		head = temp->next;
		free(temp);
	}
}

void deleteLast(){
	if(head == NULL || head->next == NULL){
		deleteFirst();
	}else{
		printf("After deleting Last Node.\n");
		emp *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}

int deleteAtPos(int pos){
	int count = countNode();
	
	if(pos <= 0 || pos > count){
		printf("Entered position is invalid.\n");
		return -1;
	}else{
		if(pos == count){
			deleteLast();
		}else if(pos == 1){
			deleteFirst();
		}else{
			emp *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}

			emp *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
		return 0;
	}
}

void main(){
	int nodes;
	printf("Enter number of nodes : \n");
	scanf("%d", &nodes);
	getchar();

	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();

	int pos;
	printf("Enter the position to delete a node:\n");
	scanf("%d", &pos);
	deleteAtPos(pos);
	printLL();
}
