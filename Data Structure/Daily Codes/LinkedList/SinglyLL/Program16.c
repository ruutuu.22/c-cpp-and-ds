/*
 * Singly Linked List
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}dm;

dm *head = NULL;

dm *createNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter the integer data : \n");
	scanf("%d", &nn->data);
	nn->next = NULL;

	return nn;
}

int countNode(){
	int count = 0;

	dm *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

void addNode(){
	dm *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void addFirst(){
	dm *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		nn->next = head;
		head = nn;
	}
}

void addLast(){
	addNode();
}

int addAtPos(int pos){
	int count = countNode();

	if(pos<=0 || pos >= count+2){
		printf("Entered position is incorrect.\n");
		return -1;
	}else{
		if(pos == count + 1){
			addLast();
		}else if(pos == 1){
			addFirst();
		}else{
			dm *nn = createNode();
			dm *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			nn->next = temp->next;
			temp->next = nn;
		}
		return 0;
	}
	
}

void deleteFirst(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if (head->next == NULL){
		free(head);
		head = NULL;
	}else{
		dm *temp = head;
		head = temp->next;
		free(temp);
	}
}

void deleteLast(){
	if(head == NULL || head->next == NULL){
		deleteFirst();
	}else{
		dm *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}

int deleteAtPos(int pos){
	int count = countNode();
	if(pos <= 0 || pos > count){
		printf("Entered position is invalid.\n");
		return -1;
	}else{
		if(pos == count){
			deleteLast();
		}else if(pos == 1){
			deleteFirst();
		}else{
			dm *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}

			dm *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
		return 0;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		dm *temp = head;
	
		while(temp->next != NULL){
			printf("{ %d } --> ", temp->data);
			temp = temp->next;
		}
		printf("{ %d }", temp->data);
		printf("\n");
	}
}


void main(){

	char choice;

	do{

		printf("Make a choice:\n");
		printf("\t1.Add a Node.\n\t2.Add at 1st Place.\n\t3.Add at position.\n\t4.Add at Last.\n\t5.Delete 1st Node\n\t6.Delete Last Node\n\t7.Delete at position.\n\t8.Count Nodes.\n\t9.Print Linked List.\n");

		int ch;
		printf("Enter your choice:\n");
		scanf("%d", &ch);

		switch(ch){
			case 1:
				addNode();
				break;

			case 2:
				addFirst();
				break;

			case 3:
				{
					int pos;
					printf("Enter the position to add a node:\n");
					scanf("%d", &pos);
					addAtPos(pos);
				}
				break;

			case 4:
				addLast();
				break;

			case 5:
				deleteFirst();
				break;

			case 6:
				deleteLast();
				break;

			case 7:
				{
					int pos;
					printf("Enter the position to delete a node:\n");
					scanf("%d", &pos);
					deleteAtPos(pos);
				}
				break;

			case 8:
				{
					int count = countNode();
					printf("Count of node is %d.\n", count);
				}
				break;
		
			case 9:
				printLL();
				break;

			default:
				printf("Invalid Choice...\n");
		}
		getchar();
		printf("Do you want to continue?...Y/N\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
