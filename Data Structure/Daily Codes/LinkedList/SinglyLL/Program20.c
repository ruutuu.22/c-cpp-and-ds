/*
 * singly linked list reverse
 */


#include<stdio.h>
#include<stdlib.h>

typedef struct demo{
	int data;
	struct demo *next;
}dm;

dm *head = NULL;

dm *createNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter the integer data : \n");
	scanf("%d", &nn->data);
	nn->next = NULL;

	return nn;
}

int countNode(){
	int count = 0;

	dm *temp = head;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

void addNode(){
	dm *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		dm *temp = head;
	
		while(temp->next != NULL){
			printf("{ %d } --> ", temp->data);
			temp = temp->next;
		}
		printf("{ %d }", temp->data);
		printf("\n");
	}
}

void reverseSLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if(head->next == NULL){
		printf("Only 1 node present.\n");
	}else{
		int count = countNode();
		dm * temp1 = head;
		//dm * temp2 = head;
	
		int cnt = count/2;
		while(cnt){
			dm *temp2 = head;
			int i=1; 
			while(i<count){
				temp2=temp2->next;
				i++;
			}
			count--;
			int temp;
			temp = temp1->data;
			temp1->data = temp2->data;
			temp2->data = temp;
			temp1=temp1->next;
			cnt--;
		}
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();

	reverseSLL();
	printLL();
}

