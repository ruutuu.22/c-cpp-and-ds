/*
 * deleteFirst()
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{
	char name[20];
	int id;
	struct Employee *next;
}emp;

emp *head = NULL;

void addNode(){
	emp *nn = (emp*)malloc(sizeof(emp));

	printf("Enter Name : \n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->name[i] = ch;
		i++;
	}

	printf("Enter ID : \n");
	scanf("%d", &nn->id);

	getchar();

	nn->next = NULL;

	if(head == NULL){
		head = nn;
	}else{
		emp *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	emp *temp = head;
	while(temp != NULL){
		printf("|| %s ", temp->name);
		printf("| %d || --> ", temp->id);
		temp = temp->next;
	}
	printf("\n");
}

void deleteFirst(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if(head->next == NULL){
		Node *temp = head;
		free(head);
		head = NULL;
	}else{
		emp *temp = head;
		head = temp->next;
		free(temp);
	}
}

void main(){
	int nodes;
	printf("Enter number of nodes : \n");
	scanf("%d", &nodes);
	getchar();

	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();
	deleteFirst();
	printf("\nAfter deleting 1st node.\n");
	printLL();
}
