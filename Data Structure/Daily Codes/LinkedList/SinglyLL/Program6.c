

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct cricLeague{
	char legName[20];
	int totalMatches;
	float priceAmt;
	struct cricLeague *next;
}cric;

cric *head = NULL;

void addNode(){
	 cric *newNode = (cric*)malloc(sizeof(cric));
	 strcpy(newNode->legName, "IPL");
	 newNode->totalMatches = 72;
	 newNode->priceAmt = 20.75;
	 newNode->next = NULL;
	 head = newNode;
}

void printLL(){
	 cric *temp = head;
	 while(temp != NULL){
		 printf("League Name = %s\n", temp->legName);
		 printf("Total Matches = %d\n", temp->totalMatches);
		 printf("Price Amount = %f\n", temp->priceAmt);
		 temp = temp->next;
	 }
}

void main(){
	 addNode(head);
	 printLL(head);
}
