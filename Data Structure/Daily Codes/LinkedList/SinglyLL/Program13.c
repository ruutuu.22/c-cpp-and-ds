/*
 * addAtPos() : with test case
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{
	char name[20];
	int id;
	struct Employee *next;
}emp;

emp *head = NULL;

emp *createNode(){
	emp *nn = (emp*)malloc(sizeof(emp));

	printf("Enter Name : \n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->name[i] = ch;
		i++;
	}

	printf("Enter ID : \n");
	scanf("%d", &nn->id);

	getchar();

	nn->next = NULL;
	return nn;
}

void addNode(){
	emp *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		emp *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void addFirst(){
	emp *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		nn->next = head;
		head = nn;
	}
}

void addLast(){
	addNode();
}

int countNodes(){
	int count = 0;
	emp *temp = head;
	while(temp != NULL){
		temp = temp->next;
		(count)++;
	}
	return count;
}


int addAtPos(int pos){
	int count = countNodes();

	if(pos<=0 || pos >= count+2){
		printf("Entered position is incorrect.\n");
		return -1;
	}else{
		if(pos == count + 1){
			addLast();
		}else if(pos == 1){
			addFirst();
		}else{
			emp *nn = createNode();
			emp *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			nn->next = temp->next;
			temp->next = nn;
		}
		return 0;
	}	
}

void printLL(){
	emp *temp = head;
	
	while(temp != NULL){
		printf("|| %s | ", temp->name);
		printf("%d || --> ", temp->id);
		temp = temp->next;
	}
	printf("\n");
}

void main(){
	int nodes;
	printf("Enter number of nodes : \n");
	scanf("%d", &nodes);
	getchar();

	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();

	int pos;
	printf("Enter position to add node:\n");
	scanf("%d", &pos);
	getchar();

	addAtPos(pos);
	printLL();
}
