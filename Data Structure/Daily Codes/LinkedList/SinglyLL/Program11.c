/*
 * createNode()
 * addFirst()
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{
	char name[20];
	int id;
	struct Employee *next;
}emp;

emp *head = NULL;

emp *createNode(){
	emp *nn = (emp*)malloc(sizeof(emp));

	printf("Enter Name : \n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->name[i] = ch;
		i++;
	}

	printf("Enter ID : \n");
	scanf("%d", &nn->id);

	getchar();

	nn->next = NULL;
	return nn;
}

void addNode(){
	emp *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		emp *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void addFirst(){
	emp *nn = createNode();

	if(head == NULL){
		head == nn;
	}else{
		nn->next = head;
		head = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		emp *temp = head;
		while(temp != NULL){
			printf("|| %s ", temp->name);
			printf("| %d || --> ", temp->id);
			temp = temp->next;
		}
		printf("\n");
	}
}

void main(){
	int nodes;
	printf("Enter number of nodes : \n");
	scanf("%d", &nodes);
	getchar();

	for(int i=1; i<=nodes; i++){
		addNode();
	}
	printLL();
	addFirst();
	printLL();
}
