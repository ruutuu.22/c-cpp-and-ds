


#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

void main(){
	node *head = NULL;

	//First node
	node *newNode = (node*)malloc(sizeof(node));
	newNode->data = 10;
	newNode->next = NULL;

	//connecting first node
	head = newNode;

	//Second node
	newNode = (node*)malloc(sizeof(node));
	newNode->data = 20;
	newNode->next = NULL;

	//connecting second node
	head->next = newNode;

	//Third node	
	newNode = (node*)malloc(sizeof(node));
	newNode->data = 30;
	newNode->next = NULL;
	
	//connecting third node
	head->next->next = newNode;

	node *temp = head;
	while(temp != NULL){
		printf("%d\n", temp->data);
		temp = temp->next;
	}
}


