/*
 * User Input
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Movie{
	char mName[20];
	float rating;
	struct Movie *next;
}mov;

mov *head = NULL;

void addNode(){
	mov *newNode = (mov*)malloc(sizeof(mov));

	printf("Enter Movie Name : \n");
	fgets(newNode->mName, 20, stdin);

	printf("Enter Rating : \n");
	scanf("%f", &(newNode->rating));

	getchar();

	newNode->next = NULL;

	if(head == NULL){
		head = newNode;
	}else{
		mov *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(){
	mov *temp = head;
	while(temp!=NULL){
		printf("|%s", temp->mName);
		printf("->%f|", temp->rating);
		temp = temp->next;
	}
}

void main(){
	addNode();
	addNode();
	addNode();
	printLL();
}
