/*
 * Implementing two stacks using SINGLE ARRAY
 */

#include<stdio.h>

int size = 0;
int top1 = 0;
int top2 = 0;
int flag1 = 0;
int flag2 = 0;

int push1(int *arr){
	if(top1 == top2 - 1){
		return -1;
	}else{
		top1++;
		printf("Enter data in STACK-1 : \n");
		scanf("%d", &arr[top1]);
		return 0;
	}
}

int push2(int *arr){
	if(top1 == top2 - 1){
		return -1;
	}else{
		top2--;
		printf("Enter data in STACK-2 : \n");
		scanf("%d", &arr[top2]);
		return 0;
	}
}

int pop1(int *arr){
	if(top1 == -1){
		flag1 = 0;
		return -1;
	}else{
		int ret = arr[top1];
		top1--;
		flag1 = 1;
		return ret;
	}
}

int pop2(int *arr){
	if(top2 == size){
		flag2 = 0;
		return -1;
	}else{
		int ret = arr[top2];
		top2++;
		flag2 = 1;
		return ret;
	}
}

void main(){
	printf("Enter the size of array:\n");
	scanf("%d", &size);

	int arr[size];
	top1 = -1;
	top2 = size;

	char choice;
	do{
		printf("Make a choice : \n");
		printf("1. PUSH-1.\n");
		printf("2. PUSH-2.\n");
		printf("3. POP-1.\n");
		printf("4. POP-2.\n");

		int ch;
		printf("Enter your choice : \n");
		scanf("%d", &ch);

		switch(ch){
			case 1:
				{
					int ret = push1(arr);
					if(ret == -1){
						printf("Stack-1 Overflow.\n");
					}
				}
				break;

			case 2:
				{
					int ret = push2(arr);
					if(ret == -1){
						printf("Stack-2 Overflow.\n");
					}
				}
				break;

			case 3:
				{
					int ret = pop1(arr);
					if(flag1 == 0){
						printf("Stack-1 Underflow.\n");
					}else{
						printf("Popped data from Stack-1 is %d.\n", ret);
					}
				}
				break;

			case 4:
				{
					int ret = pop2(arr);
					if(flag2 == 0){
						printf("Stack-2 Underflow.\n");
					}else{
						printf("Popped data from Stack-2 is %d.\n", ret);
					}
				}
				break;

			default :
				printf("Invalid Choice.\n");
		}

		getchar();
		printf("Do you want to continue?....Y/N.\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');

}
