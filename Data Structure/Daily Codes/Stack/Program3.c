/*
 * Stack implementation using array
 */

#include<stdio.h>
#include<stdbool.h>

int top = -1;
int size = 0;
int flag = 0;

bool isFull(){
	if(top == size-1){
		return true;
	}else{
		return false;
	}
}

int push(int *stack){
	if(isFull()){
		return -1;
	}else{
		top++;
		printf("Enter the data in stack:\n");
		scanf("%d", &(*(stack + top)));
		return 0;
	}
}

bool isEmpty(){
	if(top == -1){
		return true;
	}else{
		return false;
	}
}

int pop(int *stack){
	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int ret = *(stack + top);
		top--;
		return ret;
	}
}

int peek(int *stack){
	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return stack[top];
	}
}

void main(){
	printf("Enter the size of STACK:\n");
	scanf("%d", &size);
	int stack[size];
	char choice;

	do{
		printf("Make a choice : \n");
		printf("\t1.PUSH.\n\t2.POP.\n\t3.PEEK.\n\t4.IsFull.\n\t5.IsEmpty.\n");
		int ch;
		printf("Enter your choice:\n");
		scanf("%d", &ch);

		switch(ch){
			case 1:
				{
					int ret = push(stack);
					if(ret == -1){
						printf("Stack Overflow.\n");
					}
				}
				break;

			case 2:
				{
					int ret = pop(stack);
					if(flag == 0){
						printf("Stack underflow.\n");
					}else{
						printf("Popped data is %d.\n", ret);
					}
				}
				break;

			case 3:
				{
					int ret = peek(stack);
					if(flag == 0){
						printf("Stack Underflow.\n");
					}else{
						printf("Data at TOP is %d.\n", ret);
					}
				}
				break;

			case 4:
				{
					if(isFull()){
						printf("Stack is Full.\n");
					}else{
						printf("Stack is not full.\n");
					}
				}
				break;

			case 5:
				{
					if(isEmpty()){
						printf("Stack is Empty.\n");
					}else{
						printf("Stack is not Empty.\n");
					}
				}
				break;

			default:
				printf("Invalid Choice.\n");
		}

		getchar();
		printf("Do you want to continue?.....Y/N.\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

