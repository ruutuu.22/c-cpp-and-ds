/* 
 * Array example
 */

#include<stdio.h>
void main(){
	int size;
	printf("Enter the size oof array:\n");
	scanf("%d", &size);

	int arr[size];
	printf("Enter Array elements:\n");
	for(int i=0; i<size; i++){
		scanf("%d", &arr[i]);
	}
	
	int add = 0;
	printf("Array elements are :\n");
	for(int i=0;  i<size; i++){
		printf("%d\n", arr[i]);
		add = add + arr[i];
	}
	printf("Addition of data in array is %d.\n", add);
}

