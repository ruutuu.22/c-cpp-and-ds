/*
 * STACK implementation in LINKED LIST
 */

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct stack{
	int data;
	struct stack *next;

}stack;

stack *head = NULL;
stack *top = NULL;

int size = 0;
int count = 0;
int flag = 0;

stack *createNode(){
	stack *nn = (stack*)malloc(sizeof(stack));

	printf("Enter integer data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;

	return nn;
}

bool isFull(){
	if(count == size){
		return true;
	}else{
		return false;
	}
}

int push(){
	if(isFull()){
		return -1;
	}else{
		stack *nn = createNode();
		if(head == NULL){
			head = nn;
			top = head;
		}else{
			top = head;
			while(top->next != NULL){
				top = top->next;
			}
			top->next = nn;
			top = top->next;	
		}
		count++;
		return 0;
	}
}

bool isEmpty(){
	if(head == NULL){
		return true;
	}else{
		return false;
	}
}

int pop(){
	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		int ret;
		if(head->next == NULL){
			ret = top->data;
			free(head);
			head = NULL;
			top = NULL;
		}else{
			ret = top->data;
			top = head;
			while(top->next->next != NULL){
				top = top->next;
			}
			free(top->next);
			top->next = NULL;
		}
		count--;
		flag = 1;
		return ret;
	}
}

int peek(){
	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return top->data;
	}
}

void main(){
	printf("Enter the size of STACK:\n");
	scanf("%d", &size);

	char choice;
	do{
		printf("Make a choice : \n");
		printf("\t1.PUSH.\n\t2.POP.\n\t3.PEEK.\n\t4.IsFull.\n\t5.IsEmpty.\n");
		int ch;
		printf("Enter your choice:\n");
		scanf("%d", &ch);

		switch(ch){
			case 1:
				{
					int ret = push();
					if(ret == -1){
						printf("Stack Overflow.\n");
					}
				}
				break;

			case 2:
				{
					int ret = pop();
					if(flag == 0){
						printf("Stack underflow.\n");
					}else{
						printf("Popped data is %d.\n", ret);
					}
				}
				break;

			case 3:
				{
					int ret = peek();
					if(flag == 0){
						printf("Stack Underflow.\n");
					}else{
						printf("Data at TOP is %d.\n", ret);
					}
				}
				break;

			case 4:
				{
					if(isFull()){
						printf("Stack is Full.\n");
					}else{
						printf("Stack is not full.\n");
					}
				}
				break;

			case 5:
				{
					if(isEmpty()){
						printf("Stack is Empty.\n");
					}else{
						printf("Stack is not Empty.\n");
					}
				}
				break;

			default:
				printf("Invalid Choice.\n");
		}

		getchar();
		printf("Do you want to continue?.....Y/N.\n");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}

