/*
 * WAP for the linked list of States in India consisting of its name, population, budget and literacy.
 * Connect 4 states in the linked list.
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct StatesInIndia{
	char sName[20];
	long population;
	double budget;
	float literacy;
	struct StatesInIndia *next;
}states;

states *head = NULL;

void addNode(){
	states *nn = (states*)malloc(sizeof(states));

	printf("Enter State Name: \n");
	fgets(nn->sName, 20, stdin);
	nn->sName[strlen(nn->sName)-1] = '\0';

	printf("Enter Population:\n");
	scanf("%ld", &(nn->population));

	printf("Enter Budget:\n");
	scanf("%lf", &(nn->budget));

	printf("Enter Literacy:\n");
	scanf("%f", &(nn->literacy));

	getchar();

	nn->next = NULL;

	if(head == NULL){
		head = nn;
	}else{
		states *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	states *temp = head;
	while(temp != NULL){
		printf("%s\t\t", temp->sName);
		printf("%ld\t\t", temp->population);
		printf("%lf\t\t", temp->budget);
		printf("%f\n", temp->literacy);
		temp = temp->next;
	}
}

void main(){
	int nodes;
	printf("Enter Number of node:\n");
	scanf("%d", &nodes);
	getchar();

	for(int i=1; i<=nodes; i++){
		printf("\n");
		addNode();
	}

	printf("Name\t\tPopulation\tBudget\t\t\tLiteracy\n");
	printLL();
}
