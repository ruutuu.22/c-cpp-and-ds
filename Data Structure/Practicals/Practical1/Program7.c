/*
 * Write a demo structure consisting of integer data.
 * Take number of nodes from the user and print the maximum integer data.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct DEMO{
	int x;
	struct DEMO *next;
}dm;

dm *head = NULL;

void addNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter integer data:\n");
	scanf("%d", &(nn->x));
	
	nn->next = NULL;
	
	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	dm *temp = head;
	while(temp != NULL){
		printf("{ %d } -> ", temp->x);
		temp = temp->next;
	}
	printf("\n");
}

void maxData(){
	dm *temp = head;
	int max = head->x;
	while(temp != NULL){
		if(max <= temp->x){
			max = temp->x;
		}
		temp = temp->next;
	}
	printf("Max Node = %d\n", max);
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		printf("\n");
		addNode();
	}
	printLL();
	maxData();
}

