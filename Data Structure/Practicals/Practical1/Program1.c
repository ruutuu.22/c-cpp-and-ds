/*
 * WAP for the linked list of malls consisting of its name, number of shops and revenue.
 * Connect 3 malls in the linked list and print their data.
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct malls{
	char mName[20];
	int no_shops;
	float rev;
	struct malls *next;
}ml;

ml *head = NULL;

void addNode(){
	ml *newNode = (ml*)malloc(sizeof(ml));

	printf("Enter Mall Name: \n");
	fgets(newNode->mName, 20, stdin);
	newNode->mName[strlen(newNode->mName)-1] = '\0';

	printf("Enter Number of Shops:\n");
	scanf("%d", &(newNode->no_shops));

	printf("Enter Revenue:\n");
	scanf("%f", &(newNode->rev));

	getchar();

	newNode->next = NULL;

	if(head == NULL){
		head = newNode;
	}else{
		ml *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = newNode;
	}
}

void printLL(){
	ml *temp = head;
	printf("\n");
	while(temp != NULL){
		printf("| %s ", temp->mName);
		printf("| %d ", temp->no_shops);
		printf("| %f | -> ", temp->rev);
		temp = temp->next;
	}
	printf("\n");
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);
	getchar();
	for(int i=1; i<= nodes; i++){
		printf("\n");
		addNode();
	}

	printLL();
}
