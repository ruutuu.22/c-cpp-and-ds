/*
 * Write a real time example for a linked list and print its data.
 * Take 5 nodes from the user.
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct cricLeague{
	char legName[20];
	int totalMatches;
	float priceAmt;
	struct cricLeague *next;
}cric;

cric *head = NULL;

void addNode(){
	 cric *newNode = (cric*)malloc(sizeof(cric));
	 
	 printf("Enter league name:\n");
	 char ch;
	 int i=0;
	 while((ch = getchar()) != '\n'){
		 newNode->legName[i] = ch;
		 i++;
	 }

	 printf("Enter total number of matches:\n");
	 scanf("%d", &newNode->totalMatches);

	 printf("Enter Price Amount:\n");
	 scanf("%f", &newNode->priceAmt);

	 getchar();

	 newNode->next = NULL;

	 if(head == NULL){
		 head = newNode;
	 }else{
		 cric *temp = head;
		 while(temp->next != NULL){
			 temp = temp->next;
		 }
		 temp->next = newNode;
	 }
}

void printLL(){
	 cric *temp = head;
	 while(temp != NULL){
		 printf("||League Name   = %s||", temp->legName);
		 printf("||Total Matches = %d||", temp->totalMatches);
		 printf("||Price Amount  = %f||", temp->priceAmt);
		 printf("\n");
		 temp = temp->next;
	 }
}

void main(){
	 int nodes;
	 printf("Enter the number of nodes:\n");
	 scanf("%d", &nodes);
	 getchar();

	 for(int i=1; i<=nodes; i++){
		 addNode();
	 }
	 printLL();
}
