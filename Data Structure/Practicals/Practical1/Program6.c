/*
 * Write a demo structure consisting of integer data.
 * Take number of nodes from the user and print the addition of the first and last node data from the above code.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct DEMO{
	int x;
	struct DEMO *next;
}dm;

dm *head = NULL;

void addNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter integer data:\n");
	scanf("%d", &(nn->x));
	
	nn->next = NULL;

	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	dm *temp = head;
	while(temp != NULL){
		printf("{ %d } -> ", temp->x);
		temp = temp->next;
	}
	printf("\n");
}

void dataAdd(){
	int add = 0;
	dm *temp = head;
	while(temp->next != NULL){
		temp = temp->next;
	}
	add = head->x + temp->x;
	printf("Addition of Integer Data is : %d\n", add);
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		printf("\n");
		addNode();
	}
	printLL();
	dataAdd();
}

