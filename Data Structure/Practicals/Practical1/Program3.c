/*
 * WAP for the linked list of Festivals in India.
 * Take input from the user in the LL and print its data.
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct festivals{
	char fName[10];
	int no_days;
	struct festivals *next;
}fest;

fest *head = NULL;

void addNode(){
	fest *nn = (fest*)malloc(sizeof(fest));

	printf("Enter festival name:\n");
	fgets(nn->fName, 20, stdin);
	nn->fName[strlen(nn->fName)-1] = '\0';

	printf("Enter the number of days the festival is celebrated:\n");
	scanf("%d", &(nn->no_days));

	getchar();

	nn->next = NULL;

	if(head == NULL){
		head = nn;
	}else{
		fest *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	fest *temp = head;
	printf("\n");
	while(temp != NULL){
		printf("| %s ", temp->fName);
		printf("| %d | -> ", temp->no_days);
		temp = temp->next;
	}
	printf("\n");
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);
	getchar();

	for(int i=1; i<=nodes; i++){
		printf("\n");
		addNode();
	}
	printLL();
}
