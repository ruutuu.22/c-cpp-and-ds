/*
 * Write a demo structure consisting of integer data.
 * Take number of nodes from the user.
 * WAP to check prime number present in the data from above nodes.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct DEMO{
	int x;
	struct DEMO *next;
}dm;

dm *head = NULL;

void addNode(){
	dm *nn = (dm*)malloc(sizeof(dm));

	printf("Enter integer data:\n");
	scanf("%d", &(nn->x));

	nn->next = NULL;

	if(head == NULL){
		head = nn;
	}else{
		dm *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	dm *temp = head;
	while(temp != NULL){
		printf("{ %d } -> ", temp->x);
		temp = temp->next;
	}
	printf("\n");
}

void primeCheck(){
	dm *temp = head;
	
	while(temp->next != NULL){
		int count = 0;
		for(int i=1; i<=temp->x; i++){
			if(temp->x%i == 0){
				count++;
			}
		}
		if(count == 2){
			printf("Prime number in LL.\n");
		}
		temp=temp->next;
	}

}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		printf("\n");
		addNode();
	}
	printLL();
	primeCheck();
}

