/* 
 * Program8.c
 * WAP that accepts a singly linear linked list from the user.
 * Take a number from the user and only keep the elements that are equal in length to that number and delete other elements.
 * And print the linked list.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	char str[20];
	struct Node *next;
}Node;

Node *head = NULL;

Node * createNode(){
	Node * nn = (Node*)malloc(sizeof(Node));

	printf("Enter str:\n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->str[i] = ch;
		i++;
	}
	nn->next = NULL;

	return nn;
}
void addNode(){
	Node *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("[ %s ] --> ", temp->str);
			temp = temp->next;
		}
		printf("[ %s ]\n", temp->str);
	}
}

void deleteFirst(){
	if(head == NULL){
		printf("LL empty.\n");
	}else if(head->next == NULL){
		free(head);
		head = NULL;
	}else{
		Node *temp = head;
		head = temp->next;
		free(temp);
	}
}

void deleteLast(){
	if(head == NULL || head->next == NULL){
		deleteFirst();
	}else{
		Node *temp = head;
		while(temp->next->next != NULL){
			temp = temp->next;
		}
		free(temp->next);
		temp->next = NULL;
	}
}

int countNode(){
	Node *temp = head;
	int count = 0;
	while(temp != NULL){
		count++;
		temp = temp->next;
	}
	return count;
}

int deleteAtPos(int pos){
	int count = countNode();

	if(pos <=0 || pos > count){
		printf("Invalid position.\n");
		return -1;
	}else{
		if(pos == 1){
			deleteFirst();
		}else if(pos == count){
			deleteLast();
		}else{
			Node *temp = head;
			while(pos - 2){
				temp = temp->next;
				pos--;
			}
			Node *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
		return 0;
	}
}

int mystrlen(char *str){
	int count = 0;
	while(*str != '\0'){
		count++;
		str++;
	}
	return count;
}

void fixLen(){
	int len;
	printf("Enter Length of string to keep:\n");
	scanf("%d", &len);

	int count =0;
	Node *temp = head;
	while(temp != NULL){
		count++;
		if(mystrlen(temp->str) != len){
			deleteAtPos(count);
			count--;
		}
		temp = temp->next;
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	getchar();
	
	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	fixLen();
	if(head != NULL){
		printLL();
	}

}
