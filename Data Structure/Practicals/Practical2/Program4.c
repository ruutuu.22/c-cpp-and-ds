/*
 * Program4.c
 * Write a program that adds the digits of a data elements from singly linear linked list and changes the data.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node *createNode(){
	Node *nn = (Node*)malloc(sizeof(Node));

	printf("Enter integer data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;
	return nn;
}

void addNode(){
	Node *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("{ %d } --> ",temp->data);
			temp = temp->next;
		}
		printf("{ %d }\n", temp->data);
	}
}

void sumElements(){
	Node *temp =head;
	while(temp != NULL){
		int add = 0;
		while(temp->data != 0){
			int rem = temp->data % 10;
			add = add + rem;
			temp->data = temp->data / 10;
		}
		temp->data = add;
		temp = temp->next;
	}
}


void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	sumElements();
	printLL();
}

