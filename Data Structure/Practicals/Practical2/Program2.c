/*
 * Program2.c
 * Write a program that searches for the second last occurrence of a particular element from a singly linear linked list
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node *createNode(){
	Node *nn = (Node*)malloc(sizeof(Node));

	printf("Enter integer data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;
	return nn;
}

void addNode(){
	Node *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("{ %d } --> ",temp->data);
			temp = temp->next;
		}
		printf("{ %d }\n", temp->data);
	}
}

void LastSecOcc(int num){
	int count = 0;
	int occ = 0;
	int last = 0;
	int LstSec;
	Node *temp = head;
	while(temp != NULL){
		count++;
		if(temp->data == num){
			LstSec = last;
			last = count;
			occ++;
		}
		temp=temp->next;
	}
	if(head == NULL){
		printf("LL empty.\n");
	}else if(occ == 0){
		printf("No occurence of %d in LL.\n", num);
	}else if(occ == 1){
		printf("Only ONE occurence of %d in LL at %d.\n", num , last);
	}else{
		printf("Last Second Occurence of %d is at %d.\n", num, LstSec);
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	if(head != NULL){
		int num;
		printf("Enter a number to find it's 1st occurence in LL : \n");
		scanf("%d", &num);
		LastSecOcc(num);
	}
}

