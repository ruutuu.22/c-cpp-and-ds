/* 
 * Program6.c
 * WAP that accepts a singly linear linked list from the user.
 * Take a number from the user and print the data of the length of that number.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	char str[20];
	struct Node *next;
}Node;

Node *head = NULL;

Node * createNode(){
	Node * nn = (Node*)malloc(sizeof(Node));

	printf("Enter str:\n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->str[i] = ch;
		i++;
	}
	nn->next = NULL;

	return nn;
}
void addNode(){
	Node *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("[ %s ] --> ", temp->str);
			temp = temp->next;
		}
		printf("[ %s ]\n", temp->str);
	}
}

int mystrlen(char * str){
	int count = 0;
	while(*str != '\0'){
		count++;
		str++;
	}
	return count;
}

void dataLenNum(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		int len;
		printf("Enter Length to print the data of that length:\n");
		scanf("%d", &len);
		while(temp != NULL){
			if(mystrlen(temp->str) == len){
				printf("[ %s ]\n", temp->str);
			}
			temp = temp->next;
		}
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	getchar();
	
	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	dataLenNum();
}
