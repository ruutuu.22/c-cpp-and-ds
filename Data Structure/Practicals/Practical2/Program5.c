/*
 * Program5.c
 * Write a program that searches all the palindrome data elements fro a singly linear linked list and print the position of palindrome data
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node *createNode(){
	Node *nn = (Node*)malloc(sizeof(Node));

	printf("Enter integer data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;
	return nn;
}

void addNode(){
	Node *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("{ %d } --> ",temp->data);
			temp = temp->next;
		}
		printf("{ %d }\n", temp->data);
	}
}

void palindromeData(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		int count = 0;
		int palindrome = 0;
		Node *temp = head;
		while(temp != NULL){
			int num1 = temp->data;
			int num2 = 0;
			count++;

			while(num1 != 0){
				int rem = num1 % 10;
				num2 = (num2 * 10) + rem;
				num1 = num1 / 10;
			}
			if(temp->data == num2){
				printf("Palindrome found at %d.\n", count);
				palindrome++;
			}
			temp = temp->next;
		}
		if(palindrome == 0){
			printf("No palindrome data in LL.\n");
		}
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	palindromeData();
}

