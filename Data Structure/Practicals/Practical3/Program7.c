/* 
 * Program7.c
 * WAP that accepts a doubly linked list from the user.
 * Reverse the data elements from the linked list.
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	char str[20];
	struct Node *next;
}Node;

Node *head = NULL;

Node * createNode(){
	Node * nn = (Node*)malloc(sizeof(Node));

	printf("Enter str:\n");
	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){
		nn->str[i] = ch;
		i++;
	}
	nn->next = NULL;

	return nn;
}
void addNode(){
	Node *nn = createNode();

	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
		nn->prev = temp;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("[ %s ] --> ", temp->str);
			temp = temp->next;
		}
		printf("[ %s ]\n", temp->str);
	}
}

char *mystrrev (char *str){
	char *temp = str;
	while(*temp != '\0'){
		temp++;
	}
	temp--;
	
	char x;
	while(str < temp){
		x = *str;
		*str = *temp;
		*temp = x;
		str++;
		temp--;
	}
}

void dataRev(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp != NULL){
			mystrrev(temp->str);
			temp = temp->next;
		}
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	getchar();
	
	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	dataRev();
	if(head != NULL){
		printLL();
	}
}
