/*
 * Program3.c
 * Write a program that searches the occurrence of a particular element from a doubly linked list
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	struct Node *prev;
	int data;
	struct Node *next;
}Node;

Node *head = NULL;

Node *createNode(){
	Node *nn = (Node*)malloc(sizeof(Node));

	printf("Enter integer data:\n");
	scanf("%d", &nn->data);
	nn->next = NULL;
	return nn;
}

void addNode(){
	Node *nn = createNode();
	if(head == NULL){
		head = nn;
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			temp = temp->next;
		}
		temp->next = nn;
		nn->prev = temp;
	}
}

void printLL(){
	if(head == NULL){
		printf("LL empty.\n");
	}else{
		Node *temp = head;
		while(temp->next != NULL){
			printf("{ %d } --> ",temp->data);
			temp = temp->next;
		}
		printf("{ %d }\n", temp->data);
	}
}

void occurrence(int num){
	int occ = 0;
	Node *temp = head;
	while(temp != NULL){
		if(temp->data == num){
			occ++;
		}
		temp=temp->next;
	}
	if(head == NULL){
		printf("LL empty.\n");
	}else if(occ == 0){
		printf("No occurence of %d in LL.\n", num);
	}else if(occ == 1){
		printf("Only ONE occurence of %d in LL.\n", num);
	}else{
		printf("Occurence of %d is %d times.\n", num, occ);
	}
}

void main(){
	int nodes;
	printf("Enter the number of nodes:\n");
	scanf("%d", &nodes);

	for(int i=1; i<=nodes; i++){
		addNode();
	}

	printLL();

	if(head != NULL){
		int num;
		printf("Enter a number to find it's occurence in LL : \n");
		scanf("%d", &num);
		occurrence(num);
	}
}

